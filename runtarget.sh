#!/bin/bash
set -e
if [ -f "targets/$2/pre.sh" ]; then "targets/$2/pre.sh"; fi
"targets/$2/shared.sh"
"./$1.sh" "$2"
"targets/$2/out.sh"
