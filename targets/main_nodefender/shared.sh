#!/bin/bash
set -e
source "targets/main_audit/shared.sh"
rsync -r -P --size-only "targets/main_nodefender/shared" "out"
