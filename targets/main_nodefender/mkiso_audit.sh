#!/bin/bash
set -e
source "targets/main_audit/mkiso_audit.sh"

mkdir -p "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/RDP Wrapper/Install"
mkdir -p "$tempDir/sources/\$OEM\$/\$1/Users/Default/Desktop"
mkdir -p "$tempDir/sources/\$OEM\$/\$1/ProgramData/librewolf/WinUpdater"
librewolf_version=$(cat "packages/pkgver/librewolf_version.txt")
librewolfupd_version=$(cat "packages/pkgver/librewolfupd_version.txt")

lndir "$(realpath "targets/main_nodefender/resources/\$OEM\$/\$\$/Setup/Scripts_audit")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit"
ln -s "$(realpath "packages/rdpwrap.ini")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/RDP Wrapper/Install/rdpwrap.ini"
ln -s "$(realpath "packages/RDPW_Installer/RDP_CnC.exe")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/RDP Wrapper/Install/RDP_CnC.exe"
ln -s "$(realpath "packages/RDPW_Installer/RDPWInst.exe")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/RDP Wrapper/RDPWInst.exe"
ln -s "$(realpath "packages/RDPW_Installer/RDP_CnC.lnk")" "$tempDir/sources/\$OEM\$/\$1/Users/Default/Desktop/RDP_CnC.lnk"
lndir "$(realpath "packages/LibreWolf-WinUpdater_$librewolfupd_version")" "$tempDir/sources/\$OEM\$/\$1/ProgramData/librewolf/WinUpdater"
ln -s "$(realpath "packages/librewolf-$librewolf_version-windows-x86_64-setup.exe")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/librewolf-$librewolf_version-windows-x86_64-setup.exe"


tempDir2=$(basename "$tempDir")
# shellcheck disable=SC2140
wine "packages/oscdimg.exe" -bootdata:2#p0,e,b"Z:\tmp\\$tempDir2\boot\etfsboot.com"#pEF,e,b"Z:\tmp\\$tempDir2\efi\Microsoft\boot\efisys_noprompt.bin" -o -m -u2 -udfver102 -lwin_audit "Z:\\tmp\\$tempDir2" "out\\win_audit.iso"

rm -rf "$tempDir"
