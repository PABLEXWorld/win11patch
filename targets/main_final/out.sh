#!/bin/bash
set -e
wimlib-imagex export "out/shared/install-new.wim" 1 "out/install.esd" --solid
rm -rf "out/shared"
source "targets/main_final/mkiso_final.sh" "$mode"
