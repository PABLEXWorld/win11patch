#!/bin/bash
set -e
source "targets/main_generic/mkiso_base.sh"

mkdir "$tempDir/sources/\$OEM\$"
lndir "$(realpath "targets/main_final/resources/\$OEM\$")" "$tempDir/sources/\$OEM\$"
ln -s "$(realpath "packages/GetTrustedInstaller.exe")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts/GetTrustedInstaller.exe"
ln -s "$(realpath "packages/MAS_AIO.cmd")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts/MAS_AIO.cmd"
ln -s "$(realpath "out/install.esd")" "$tempDir/sources"
ln -s "$(realpath "targets/main_final/resources/Autounattend.xml")" "$tempDir"
ln -s "$(realpath "targets/main_final/resources/upgrade.bat")" "$tempDir/upgrade.cmd"

tempDir2=$(basename $tempDir)
# shellcheck disable=SC2140
wine "packages/oscdimg.exe" -bootdata:2#p0,e,b"Z:\tmp\\$tempDir2\boot\etfsboot.com"#pEF,e,b"Z:\tmp\\$tempDir2\efi\Microsoft\boot\efisys_noprompt.bin" -o -m -u2 -udfver102 -lCCCOMA_X64FRE_ES-MX_DV9 "Z:\\tmp\\$tempDir2" "out\\win_final.iso"

rm -rf "$tempDir"
