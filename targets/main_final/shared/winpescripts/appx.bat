@echo off
setlocal enabledelayedexpansion
set appsdir=Apps
set ImagePath=C:\
if exist "!appsdir!\MSIXFramework\*" for /f "tokens=* delims=" %%# in ('dir /b /a:-d "!appsdir!\MSIXFramework\*.*x"') do (
    echo %%~n#
    dism /Image:C:\ /Add-ProvisionedAppxPackage /PackagePath:"!appsdir!\MSIXFramework\%%#" /SkipLicense
)
for /f "tokens=1 eol=# delims=" %%# in (CustomAppsList.txt) do call :appx_add "%%#"
endlocal
goto :eof

:appx_add
set "featurepath=!appsdir!\%~1"
if not exist "%featurepath%\License.xml" goto :eof
if not exist "%featurepath%\*.appx*" if not exist "%featurepath%\*.msix*" goto :eof
set "main="
if not defined main if exist "%featurepath%\*.msixbundle" for /f "tokens=* delims=" %%# in ('dir /b /a:-d "%featurepath%\*.msixbundle"') do set "main=%%#"
if not defined main if exist "%featurepath%\*.appxbundle" for /f "tokens=* delims=" %%# in ('dir /b /a:-d "%featurepath%\*.appxbundle"') do set "main=%%#"
if not defined main if exist "%featurepath%\*.appx" for /f "tokens=* delims=" %%# in ('dir /b /a:-d "%featurepath%\*.appx"') do set "main=%%#"
if not defined main if exist "%featurepath%\*.msix" for /f "tokens=* delims=" %%# in ('dir /b /a:-d "%featurepath%\*.msix"') do set "main=%%#"
if not defined main goto :eof
echo %featurepath%...
dism /Image:%ImagePath% /Add-ProvisionedAppxPackage /PackagePath:"%featurepath%\%main%" /LicensePath:"%featurepath%\License.xml" /Region:all
goto :eof
