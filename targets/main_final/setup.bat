call winpescripts\appx.bat                                      || exit /b 1
reg load HKLM\DEFAULT C:\Users\Default\NTUSER.DAT               || exit /b 1
reg load HKLM\SOFTWARE2 C:\Windows\System32\config\SOFTWARE     || exit /b 1
reg load HKLM\SYSTEM2 C:\Windows\System32\config\SYSTEM         || exit /b 1
call winpescripts\regmod.bat offlineServicing                   || exit /b 1
reg unload HKLM\DEFAULT                                         || exit /b 1
reg unload HKLM\SOFTWARE2                                       || exit /b 1
reg unload HKLM\SYSTEM2                                         || exit /b 1
rd /S /Q C:\Boot                                                || exit /b 1
rd /S /Q C:\Users\Administrador                                 || exit /b 1
rd /S /Q C:\Windows\Panther                                     || exit /b 1
attrib -R -H -S C:\bootmgr                                      || exit /b 1
attrib -R -H -S C:\BOOTSECT.BAK                                 || exit /b 1
attrib -H -S C:\BOOTNXT                                         || exit /b 1
del C:\bootmgr                                                  || exit /b 1
del C:\BOOTSECT.BAK                                             || exit /b 1
del C:\BOOTNXT                                                  || exit /b 1
icacls C:\hiberfil.sys /reset                                   || exit /b 1
attrib -H -S C:\hiberfil.sys                                    || exit /b 1
attrib -H -S C:\pagefile.sys                                    || exit /b 1
attrib -H -S C:\swapfile.sys                                    || exit /b 1
del C:\hiberfil.sys                                             || exit /b 1
del C:\pagefile.sys                                             || exit /b 1
del C:\swapfile.sys                                             || exit /b 1
del C:\Windows\SystemTemp\*.evtx                                || exit /b 1
reg load HKLM\SOFTWARE2 C:\Windows\System32\config\SOFTWARE     || exit /b 1
start /wait regedit.exe
reg unload HKLM\SOFTWARE2                                       || exit /b 1
xcopy /E "@DISMPP_VERSION@" X:                                || exit /b 1
start /wait Dism++x64.exe
