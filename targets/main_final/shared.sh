#!/bin/bash
set -e
source "targets/main_generic/shared.sh"
source "targets/main_final/appx_sort.sh" "$mode"
source "targets/winpe/shared.sh"
rsync -r -P --size-only "targets/main_final/shared" "out"
rsync -r -P --size-only "out/Apps" "out/shared"

dismpp_version=$(urlencode -d < "packages/pkgver/dismpp_filename.txt")
rsync -r -P --size-only "packages/${dismpp_version%.zip}" "out/shared"
