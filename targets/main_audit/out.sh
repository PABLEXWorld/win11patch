#!/bin/bash
set -e
rm "out/install.wim"
mv "out/shared/install.wim" "out"
mv "out/shared/ActionFiles" "out/ActionFiles"
rm -rf "out/shared"
source "targets/main_$mode/mkiso_audit.sh"
