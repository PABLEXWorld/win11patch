#!/bin/bash
set -e
source "targets/main_generic/mkiso_base.sh"

mkdir -p "$tempDir/sources/\$OEM\$/\$progs/ExplorerPatcher"
mkdir -p "$tempDir/sources/\$OEM\$/\$\$/SystemApps/Microsoft.Windows.StartMenuExperienceHost_cw5n1h2txyewy"
mkdir -p "$tempDir/sources/\$OEM\$/\$\$/SystemApps/ShellExperienceHost_cw5n1h2txyewy"
mkdir -p "$tempDir/sources/\$OEM\$/\$\$/System32/drivers/etc"

lndir "$(realpath "targets/main_audit/resources/\$OEM\$")" "$tempDir/sources/\$OEM\$"
lndir "$(realpath "packages/ep_setup")" "$tempDir/sources/\$OEM\$/\$progs/ExplorerPatcher"
ln -s "$(realpath "packages/ep_setup.exe")" "$tempDir/sources/\$OEM\$/\$progs/ExplorerPatcher"
ln -s "$(realpath "packages/ep_setup/ExplorerPatcher.amd64.dll")" "$tempDir/sources/\$OEM\$/\$\$/dxgi.dll"
ln -s "$(realpath "packages/ep_setup/ExplorerPatcher.amd64.dll")" "$tempDir/sources/\$OEM\$/\$\$/SystemApps/Microsoft.Windows.StartMenuExperienceHost_cw5n1h2txyewy/dxgi.dll"
ln -s "$(realpath "packages/ep_setup/ExplorerPatcher.amd64.dll")" "$tempDir/sources/\$OEM\$/\$\$/SystemApps/ShellExperienceHost_cw5n1h2txyewy/dxgi.dll"
ln -s "$(realpath "packages/ep_setup/wincorlib.dll")" "$tempDir/sources/\$OEM\$/\$\$/SystemApps/Microsoft.Windows.StartMenuExperienceHost_cw5n1h2txyewy"
rm "$tempDir/sources/\$OEM\$/\$progs/ExplorerPatcher/wincorlib.dll"
ln -s "$(realpath "out/hosts")" "$tempDir/sources/\$OEM\$/\$\$/System32/drivers/etc"

msoffice_version=$(cat "packages/pkgver/msoffice_version.txt")
mkdir -p "$tempDir/sources/\$OEM\$/\$\$/Setup/es-MX_Office_Current_x64_v$msoffice_version"
lndir "$(realpath "packages/es-ES_Office_Current_x64_v$msoffice_version")" "$tempDir/sources/\$OEM\$/\$\$/Setup/es-MX_Office_Current_x64_v$msoffice_version"

mkdir -p "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/DX"
sevenzip_version=$(cat "packages/pkgver/7z_version.txt")
sticky_filename=$(basename "$(ls packages/ClassicStickyNotes*)")
wrar_version=$(cat "packages/pkgver/wrar_version.txt")
ptoys_filename=$(cat "packages/pkgver/ptoys_filename.txt")
ptoys_version=$(cat "packages/pkgver/ptoys_version.txt")
librewolf_version=$(cat "packages/pkgver/librewolf_version.txt")
chromium_version=$(cat "packages/pkgver/chromium_version.txt")

lndir "$(realpath "packages/DX")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/DX"
ln -s "$(realpath "packages/7z$sevenzip_version-x64.exe")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/7z$sevenzip_version-x64.exe"
ln -s "$(realpath "packages/GetTrustedInstallerc.exe")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/GetTrustedInstallerc.exe"
ln -s "$(realpath "packages/vc_redist.x64.exe")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/vc_redist.x64.exe"
ln -s "$(realpath "packages/$ptoys_filename")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/$ptoys_filename"
ln -s "$(realpath "packages/$sticky_filename")" "$tempDir/sources/\$OEM\$/\$\$/Setup/$sticky_filename"

rm "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/setup.bat"
rm "$tempDir/sources/\$OEM\$/\$1/Users/Default/AppData/Local/Microsoft/PowerToys/settings.json"
cp "targets/main_audit/resources/Autounattend.xml" "$tempDir/Autounattend.xml"
cp "targets/main_audit/resources/\$OEM\$/\$\$/Setup/Scripts_audit/setup.bat" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/setup.bat"
cp "targets/main_audit/resources/\$OEM\$/\$1/Users/Default/AppData/Local/Microsoft/PowerToys/settings.json" "$tempDir/sources/\$OEM\$/\$1/Users/Default/AppData/Local/Microsoft/PowerToys"

sed -i "s/@MSOFFICE_VERSION@/${msoffice_version}/g" "$tempDir/Autounattend.xml"
sed -i "s/@STICKY_FILENAME@/${sticky_filename}/g" "$tempDir/Autounattend.xml"
sed -i "s/@SEVENZIP_VERSION@/${sevenzip_version}/g" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/setup.bat"
sed -i "s/@PTOYS_VERSION@/${ptoys_version}/g" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/setup.bat"
sed -i "s/@WRAR_VERSION@/${wrar_version}/g" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/setup.bat"
sed -i "s/@PTOYS_VERSION@/${ptoys_version}/g" "$tempDir/sources/\$OEM\$/\$1/Users/Default/AppData/Local/Microsoft/PowerToys/settings.json"
sed -i "s/@LIBREWOLF_VERSION@/${librewolf_version}/g" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/setup.bat"
sed -i "s/@CHROMIUM_VERSION@/${chromium_version}/g" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/setup.bat"

mkdir -p "$tempDir/sources/\$OEM\$/\$1/ProgramData/PS-AdBlock"
lndir "$(realpath "tools/PS-AdBlock")" "$tempDir/sources/\$OEM\$/\$1/ProgramData/PS-AdBlock"

mkdir -p "$tempDir/sources/\$OEM\$/\$\$/System32/Sysprep/ActionFiles"
for file in out/ActionFiles/*.xml; do
  sed 's/<?xml version="1.0" encoding="utf-8"?>/<?xml version="1.0"?>/g' "$file" | xmllint --format - > "$tempDir/sources/\$OEM\$/\$\$/System32/Sysprep/ActionFiles/$(basename "$file")"
done
mkdir "$tempDir/sources/\$OEM\$/\$\$/System32/Recovery"
ln -s "$(realpath "out/winre.wim")" "$tempDir/sources/\$OEM\$/\$\$/System32/Recovery/winre.wim"
ln -s "$(realpath "out/install.wim")" "$tempDir/sources/install.wim"

mkdir -p "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/Edge"
lndir "$(realpath "out/Edge")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/Edge"
