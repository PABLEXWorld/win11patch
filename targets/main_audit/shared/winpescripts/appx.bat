@echo off
setlocal enabledelayedexpansion
set appsdir=Apps
set ImagePath=C:\mount
call :appx_add "Microsoft.SecHealthUI_8wekyb3d8bbwe"
endlocal
goto :eof

:appx_add
set "featurepath=!appsdir!\%~1"
if not exist "%featurepath%\License.xml" goto :eof
if not exist "%featurepath%\*.appx*" if not exist "%featurepath%\*.msix*" goto :eof
set "main="
if not defined main if exist "%featurepath%\*.msixbundle" for /f "tokens=* delims=" %%# in ('dir /b /a:-d "%featurepath%\*.msixbundle"') do set "main=%%#"
if not defined main if exist "%featurepath%\*.appxbundle" for /f "tokens=* delims=" %%# in ('dir /b /a:-d "%featurepath%\*.appxbundle"') do set "main=%%#"
if not defined main if exist "%featurepath%\*.appx" for /f "tokens=* delims=" %%# in ('dir /b /a:-d "%featurepath%\*.appx"') do set "main=%%#"
if not defined main if exist "%featurepath%\*.msix" for /f "tokens=* delims=" %%# in ('dir /b /a:-d "%featurepath%\*.msix"') do set "main=%%#"
if not defined main goto :eof
echo %featurepath%...
dism /Image:%ImagePath% /Add-ProvisionedAppxPackage /PackagePath:"%featurepath%\%main%" /LicensePath:"%featurepath%\License.xml" /Region:all
goto :eof
