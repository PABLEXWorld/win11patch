set driveletter=C:
set mount=%driveletter%\mount
copy tools\fzf.exe X:
copy tools\awk.exe X:
cd /D %mount%
for /F "tokens=1 eol=# delims=" %%i in (%~dp0fzfterms.txt) do (
fzf.exe -e -f %%i >> Z:\fzf_list.txt
)
awk.exe "!/FileMaps/" Z:\fzf_list.txt > Z:\fzf_list_cleaned1.txt
awk.exe "!/WinSxS/" Z:\fzf_list_cleaned1.txt > Z:\fzf_list_cleaned2.txt
awk.exe "!/MSRAW/" Z:\fzf_list_cleaned2.txt > Z:\fzf_list_cleaned3.txt
awk.exe "!/msrating/" Z:\fzf_list_cleaned3.txt > Z:\fzf_list_cleaned.txt

cd /D Z:\
del fzf_list.txt
del fzf_list_cleaned1.txt
del fzf_list_cleaned2.txt
del fzf_list_cleaned3.txt

for /f "tokens=1 delims=" %%i in (fzf_list_cleaned.txt) do del "%mount%\%%i"

for /f %%i in ('dir /a:-d /s /b %mount%\Windows\System32\wups*') do del %%i
for /f %%i in ('dir /a:d /s /b %mount%\Windows\SystemApps\*CloudExperienceHost*') do rd /s /q %%i
rd /S /Q %mount%\Windows\diagnostics\system\Apps
rd /S /Q %mount%\Windows\diagnostics\system\WindowsUpdate
