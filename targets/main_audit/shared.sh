#!/bin/bash
set -e
mkdir -p "out/shared/tools"
source "targets/main_generic/shared.sh"
source "targets/main_audit/appx_sort.sh"
source "targets/main_audit/dove.sh"
source "targets/winpe/shared.sh"
rsync -r -P --size-only "targets/main_audit/shared" "out"
mv "out/Apps" "out/shared"

cp "packages/DeleteTasks.exe" "out/shared/tools"
cp "packages/FixSysprep.exe" "out/shared/tools"
cp "packages/install_wim_tweak.exe" "out/shared/tools"
cp "packages/ToolKitHelper.exe" "out/shared/tools"
cp "packages/awk.exe" "out/shared/tools"
cp "packages/fzf.exe" "out/shared/tools"

IFS='.' splitver=($(cat "out/isodir.txt")); unset IFS
os_build="10.0.${splitver[0]}.${splitver[1]}"

IFS='_' splitver=($(cat "packages/pkgver/ep_version.txt")); unset IFS
ep_version=${splitver[0]}
IFS='.' splitver=($ep_version); unset IFS

sed -i "s/@OS_BUILD@/${os_build}/g" "out/shared/regmod_offlineServicing/Custom/ExplorerPatcher.reg"
sed -i "s/@EP_VERSION@/${ep_version}/g" "out/shared/regmod_offlineServicing/Custom/ExplorerPatcher.reg"
sed -i "s/@EP_VERSION_MAJOR@/$(printf "%08x" ${splitver[2]})/g" "out/shared/regmod_offlineServicing/Custom/ExplorerPatcher.reg"
sed -i "s/@EP_VERSION_MINOR@/$(printf "%08x" ${splitver[3]})/g" "out/shared/regmod_offlineServicing/Custom/ExplorerPatcher.reg"

regpath='HKEY_LOCAL_MACHINE\DEFAULT\SOFTWARE\ExplorerPatcher'
regpath_startmenu='HKEY_LOCAL_MACHINE\DEFAULT\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\ExplorerPatcher'
files=(explorer twinui.pcshell startdocked startui)
regpaths=("$regpath\\explorer" "$regpath\\twinui.pcshell" "$regpath_startmenu\\StartDocked" "$regpath_startmenu\\StartUI")

echo "Windows Registry Editor Version 5.00" > "out/shared/regmod_offlineServicing/Custom/ExplorerPatcher_symbols.reg"
echo >> "out/shared/regmod_offlineServicing/Custom/ExplorerPatcher_symbols.reg"

for key1 in "${!files[@]}"
do
  IFS=$'\n' symbols=($(cat "out/symbols/symbols_${files[$key1]}.csv")); unset IFS

  echo "[${regpaths[$key1]}]" >> "out/shared/regmod_offlineServicing/Custom/ExplorerPatcher_symbols.reg"
  for key2 in "${!symbols[@]}"
  do
    IFS=',' symline=(${symbols[$key2]}); unset IFS
    echo "${symline[3]}=dword:$(printf "%08x" 0x${symline[0]:1:-1})" >> "out/shared/regmod_offlineServicing/Custom/ExplorerPatcher_symbols.reg"
  done

  if [ ${#files[@]} -ne $((key1+1)) ]; then echo >> "out/shared/regmod_offlineServicing/Custom/ExplorerPatcher_symbols.reg"; fi
done
unix2dos "out/shared/regmod_offlineServicing/Custom/ExplorerPatcher_symbols.reg"
