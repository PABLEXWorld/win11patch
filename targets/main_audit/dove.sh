#!/bin/bash
set +e
UUP_CONVERTER_SCRIPT=1
source "tools/uupconv/uupconv_getfirstmeta.sh"
cp "packages/uupconv/convert_ve_plugin" "out"
sed -i "s/ISODIR\/sources\/install.\$type \"\$newName\"/shared\/install.wim \"\$newName\"/g" "out/convert_ve_plugin"
source "out/convert_ve_plugin"
type="wim"
pushd out || exit

ln -s "$(realpath "install.wim")" "ISODIR/sources/install.wim"
echo -e "$infoColor""Adding Enterprise edition...""$resetColor"
createVirtualEdition "Enterprise"
error=$?
errorHandler $error "Failed to create virtual edition"
set -e
popd
rm "out/convert_ve_plugin"
rm "out/ISODIR/sources/install.wim"
