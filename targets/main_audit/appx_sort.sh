#!/bin/bash
lang="es-es"
appsFolder="out/Apps"

function parsePayloads () {
    for payload in $payloads
    do
        local filename
        filename=$(basename "$(echo "${payload:28:-1}" | sed 's/\\/\//g' -)")
        if [ ! -f "$featurepath/$filename" ]; then
            if [ "$featuretype" == "MSIXFramework" ]; then echo "$1"...; fi
            cp "$UUPs_apps/$filename" "$featurepath/$filename"
        fi
    done
}

function parsePackages () {
    for package in $packages
    do
        set +e
        local language
        language=$(cat "compdb.xml" | xmllint --xpath "/CompDB/Packages/Package[@ID=\"${package:4:-1}\"]/SatelliteInfo/ApplyToInfo/ApplyTo/@Value" - 2> /dev/null)
        if [[ -n $language && ! $language =~ $lang ]]; then continue; fi
        local language=
        set -e

        local payloads
        payloads=$(cat "compdb.xml" | xmllint --xpath "/CompDB/Packages/Package[@ID=\"${package:4:-1}\"]/Payload/PayloadItem/@Path" -)
        parsePayloads "$1"
    done
}

function parseFeature () {
    local featuretype
    featuretype=$(cat "compdb.xml" | xmllint --xpath "string(/CompDB/Features/Feature[@FeatureID=\"$1\"]/@Type)" -)
    if [ "$featuretype" != "MSIXFramework" ]; then
        local featurepath="$appsFolder/$1"
        if [ ! -d "$featurepath" ]; then
            echo "$1..."
            mkdir "$featurepath"
        fi
        local license
        local deps
        local packages
        license=$(cat "compdb.xml" | xmllint --xpath "/CompDB/Features/Feature[@FeatureID=\"$1\"]/CustomInformation/CustomInfo[@Key=\"licensedata\"]/text()" - 2> /dev/null)
        if [ -n "$license" ]; then echo "$license" | sed -e 's/<!\[CDATA\[//g; s/\]\]>//g' - > "$featurepath/License.xml"; fi
        packages=$(cat "compdb.xml" | xmllint --xpath "/CompDB/Features/Feature[@FeatureID=\"$1\"]/Packages/Package[not(contains(@PackageType,'Stub'))]/@ID" -)
        parsePackages "$1"
        deps=$(cat "compdb.xml" | xmllint --xpath "/CompDB/Features/Feature[@FeatureID=\"$1\"]/Dependencies/Feature/@FeatureID" - 2> /dev/null)
            for dep in $deps
        do
            parseFeature "${dep:11:-1}"
        done
    fi
}

function ensureAppsFolder () {
    if [ -d "$appsFolder" ]; then rm -rf "$appsFolder"; fi
    mkdir "$appsFolder"
}

7z e "$UUPs_apps/*.AggregatedMetadata.cab" "DesktopTargetCompDB_App_Neutral.xml.cab" > "/dev/null"
7z e "DesktopTargetCompDB_App_Neutral.xml.cab" > "/dev/null"
rm "DesktopTargetCompDB_App_Neutral.xml.cab"

sed 's/<CompDB .*>/<CompDB>/g' "DesktopTargetCompDB_App_Neutral.xml" > "compdb.xml"
rm "DesktopTargetCompDB_App_Neutral.xml"

ensureAppsFolder

parseFeature "Microsoft.SecHealthUI_8wekyb3d8bbwe"

rm "compdb.xml"
