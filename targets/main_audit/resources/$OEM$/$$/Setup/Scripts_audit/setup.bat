cd %~dp0
set sevenzipver=@SEVENZIP_VERSION@
set ptoysver=@PTOYS_VERSION@
set wrarver=@WRAR_VERSION@
set librewolfver=@LIBREWOLF_VERSION@
set chromiumver=@CHROMIUM_VERSION@

Edge\setup.exe --uninstall --system-level --verbose-logging --force-uninstall
Edge\setup.exe --uninstall --msedgewebview --system-level --verbose-logging --force-uninstall

regsvr32 /s "%ProgramFiles%\ExplorerPatcher\ep_weather_host.dll" || pause && exit /b 3
regsvr32 /s "%ProgramFiles%\ExplorerPatcher\ep_weather_host_stub.dll" || pause && exit /b 3
regsvr32 /s "%ProgramFiles%\ExplorerPatcher\ExplorerPatcher.amd64.dll" || pause && exit /b 3

DX\DXSETUP.exe /silent || pause && exit /b 3

.\7z%sevenzipver%-x64.exe /S || pause && exit /b 3

.\VC_redist.x64.exe /install /quiet /norestart || pause && exit /b 3

PowerShell.exe -ExecutionPolicy Bypass -Command "& {Get-NetFirewallRule -Enabled True | Disable-NetFirewallRule}" || pause && exit /b 3

IF EXIST PowerToysSetup-%ptoysver%-x64.exe (
    .\PowerToysSetup-%ptoysver%-x64.exe /install /quiet /norestart || pause && exit /b 3

    ren "%ProgramFiles%\PowerToys\ImageResizerContextMenuPackage.msix" ImageResizerContextMenuPackage.msix.BAK || pause && exit /b 3
    ren "%ProgramFiles%\PowerToys\WinUI3Apps\PowerRenameContextMenuPackage.msix" PowerRenameContextMenuPackage.msix.BAK || pause && exit /b 3

    PowerShell.exe -ExecutionPolicy Bypass -Command "& {foreach ($file in Get-ChildItem -Recurse \"C:\Program Files\PowerToys\" -include *.exe) {New-NetFirewallRule -Program \"$file\" -DisplayName \"$file\" -Profile @('Domain', 'Private', 'Public') -Direction Outbound -Action Block}}" || pause && exit /b 3
)

IF EXIST winrar-x64-%wrarver%es.exe (
    .\winrar-x64-%wrarver%es.exe /S || pause && exit /b 3

    PowerShell.exe -ExecutionPolicy Bypass -Command "& {Get-AppXPackage -AllUsers *WinRAR* | Remove-AppXPackage -AllUsers}" || pause && exit /b 3
)

IF EXIST librewolf-%librewolfver%-windows-x86_64-setup.exe (
    .\librewolf-%librewolfver%-windows-x86_64-setup.exe /S || pause && exit /b 3
    schtasks /create /xml "LibreWolf WinUpdater.xml" /tn "LibreWolf WinUpdater" /ru SYSTEM /it || pause && exit /b 3
)

IF EXIST ungoogled-chromium_%chromiumver%_installer_x64.exe (
    .\ungoogled-chromium_%chromiumver%_installer_x64.exe --system-level || pause && exit /b 3
    schtasks /create /xml "Ungoogled Chromium Update.xml" /tn "Ungoogled Chromium Update" /ru SYSTEM /it || pause && exit /b 3
)

schtasks /create /xml "PS-AdBlock.xml" /tn "PS-AdBlock" /ru SYSTEM /it || pause && exit /b 3

IF EXIST "RDP Wrapper\setup.bat" (
    call "RDP Wrapper\setup.bat" || pause && exit /b 3
)

call "%windir%\Setup\regmod.bat" audit || pause && exit /b 3
