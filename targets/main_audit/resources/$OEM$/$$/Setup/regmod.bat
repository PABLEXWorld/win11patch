@echo off
setlocal enabledelayedexpansion
cd %~dp0
reg load HKLM\DEFAULT "%SystemDrive%\Users\Default\NTUSER.DAT"          || exit /b 1
for /f "tokens=1 delims=" %%i in ('dir /S /B "%~dp0regmod_%1"') do (
    if %%~xi==.reg (
        set filename=%%~ni
        if not "!filename:~0,1!"=="#" (
            echo %%~nxi
            reg import "%%i"                                            || exit /b 1
        )
    )
)                                                                       || exit /b 1
reg unload HKLM\DEFAULT                                                 || exit /b 1
endlocal
echo on
