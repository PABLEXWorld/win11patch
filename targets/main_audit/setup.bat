diskpart /s winpescripts\diskpart_script.txt                                                                                                            || exit /b 1
set driveletter=C:
set mount=%driveletter%\mount
set scratch=%driveletter%\scratch
md %scratch%                                                                                                                                            || exit /b 1
md %mount%                                                                                                                                              || exit /b 1
dism /Mount-Wim /WimFile:install.wim /index:1 /MountDir:%mount%                                                                                         || exit /b 1
mklink /H %mount%\Windows\SystemApps\Microsoft.Windows.StartMenuExperienceHost_cw5n1h2txyewy\wincorlib_orig.dll %mount%\Windows\System32\wincorlib.dll  || exit /b 1

call appx.bat                                                                                                                                           || exit /b 1

copy tools\ToolKitHelper.exe X:                                                                                                                         || exit /b 1
ToolKitHelper.exe %mount% winpescripts\RemovePkgsList.txt
if ERRORLEVEL 1 (exit /b 1)

if not exist %mount%\ProgramData\Microsoft\Windows\AppxProvisioning.xml (exit /b 1)
del %mount%\ProgramData\Microsoft\Windows\AppxProvisioning.xml

copy tools\install_wim_tweak.exe X:                                                                                                                     || exit /b 1
install_wim_tweak.exe /p %mount% /l                                                                                                                     || exit /b 1
install_wim_tweak.exe /p %mount% /c "Microsoft-Windows-AppManagement" /n /r                                                                             || exit /b 1
install_wim_tweak.exe /p %mount% /c "Microsoft-Windows-Kernel-LA57" /n /r                                                                               || exit /b 1
install_wim_tweak.exe /p %mount% /c "Microsoft-Windows-PeerDist" /n /r                                                                                  || exit /b 1
install_wim_tweak.exe /h /p %mount% /l                                                                                                                  || exit /b 1
dism /Image:%mount% /Cleanup-Image /StartComponentCleanup /ResetBase /ScratchDir:%scratch%                                                              || exit /b 1
reg load HKLM\DEFAULT %mount%\Users\Default\NTUSER.DAT                                                                                                  || exit /b 1
reg load HKLM\SOFTWARE2 %mount%\Windows\System32\config\SOFTWARE                                                                                        || exit /b 1
reg load HKLM\SYSTEM2 %mount%\Windows\System32\config\SYSTEM                                                                                            || exit /b 1
call regmod.bat offlineServicing                                                                                                                        || exit /b 1
call remove.bat
copy tools\DeleteTasks.exe X:                                                                                                                           || exit /b 1
DeleteTasks.exe                                                                                                                                         || exit /b 1
copy tools\FixSysprep.exe X:                                                                                                                            || exit /b 1
FixSysprep.exe                                                                                                                                          || exit /b 1
reg unload HKLM\DEFAULT                                                                                                                                 || exit /b 1
reg unload HKLM\SOFTWARE2                                                                                                                               || exit /b 1
reg unload HKLM\SYSTEM2                                                                                                                                 || exit /b 1
echo. > commit                                                                                                                                          || exit /b 1
dism /Unmount-Image /MountDir:%mount% /Commit                                                                                                           || exit /b 1
del commit
