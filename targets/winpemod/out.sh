#!/bin/bash
set -e
wimlib-imagex export "out/shared/winpemod.wim" 1 "out/winpe.wim" --compress=maximum --boot
rm -rf "out/shared"
rm -rf "out/regmod_winpe"
rm -rf "out/UUPs_esd"
rm -rf "out/winpemod"
rm "out/winpe.iso"
source "targets/winpe/mkiso_winpe.sh" winpe
