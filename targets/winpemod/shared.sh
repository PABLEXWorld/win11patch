#!/bin/bash
set -e
source "targets/winpe/shared.sh"
rsync -r -P --size-only "out/regmod_winpe" "out/shared"
rsync -r -P --size-only "out/winpemod" "out/shared"
