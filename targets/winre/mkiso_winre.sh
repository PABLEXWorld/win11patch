#!/bin/bash
set -e

tempDir=`mktemp -d`

mkdir -p "$tempDir/boot" "$tempDir/efi"
lndir "$(realpath "out/ISODIR/boot")" "$tempDir/boot"
lndir "$(realpath "out/ISODIR/efi")" "$tempDir/efi"
mkdir "$tempDir/sources"
ln -s "$(realpath "out/winre.wim")" "$tempDir/sources/boot.wim"
ln -s "$(realpath "out/ISODIR/bootmgr")" "$tempDir/bootmgr"
ln -s "$(realpath "out/ISODIR/bootmgr.efi")" "$tempDir/bootmgr.efi"

tempDir2=$(basename $tempDir)
# shellcheck disable=SC2140
wine "packages/oscdimg.exe" -bootdata:2#p0,e,b"Z:\tmp\\$tempDir2\boot\etfsboot.com"#pEF,e,b"Z:\tmp\\$tempDir2\efi\Microsoft\boot\efisys_noprompt.bin" -o -m -u2 -udfver102 -lWinRE11 "Z:\\tmp\\$tempDir2" "out\\WinRE11.iso"

rm -rf "$tempDir"
