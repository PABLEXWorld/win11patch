#!/bin/bash
set -e
source "targets/main_audit/shared.sh"
rsync -r -P --size-only "targets/main_windefend/shared" "out"

chromiumwebstore_version=$(cat "packages/pkgver/chromiumwebstore_version.txt")
ublock_version=$(cat "packages/pkgver/ublock_version.txt")
sed -i "s/@CHROMIUMWEBSTORE_VERSION@/${chromiumwebstore_version}/g" "out/shared/regmod_offlineServicing/Custom/Install Chromium Extensions.reg"
sed -i "s/@UBLOCK_VERSION@/${ublock_version}/g" "out/shared/regmod_offlineServicing/Custom/Install Chromium Extensions.reg"
