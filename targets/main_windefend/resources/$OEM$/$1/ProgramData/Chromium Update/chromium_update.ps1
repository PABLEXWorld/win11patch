$ProgressPreference = 'SilentlyContinue'
$relinfo =  ConvertFrom-Json (
                Invoke-WebRequest "https://api.github.com/repos/ungoogled-software/ungoogled-chromium-windows/releases/latest" |
                Select-Object -ExpandProperty Content
            )
$chromium_latest = ($relinfo.tag_name -Split "-")[0]
$chromium_current = Get-ItemProperty -Path HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\Chromium -Name Version |
                    Select-Object -ExpandProperty Version
if (!$chromium_current.Equals($chromium_latest)) {
    $url = $relinfo.assets[0].browser_download_url
    $filename = $relinfo.assets[0].name
    Invoke-WebRequest $url -OutFile "C:\Windows\Setup\$filename"
    Start-Process "C:\Windows\Setup\$filename" -ArgumentList "--system-level" -Wait
    Remove-Item -Path "C:\Windows\Setup\$filename"
}