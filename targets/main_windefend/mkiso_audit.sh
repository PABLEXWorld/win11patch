#!/bin/bash
set -e
source "targets/main_audit/mkiso_audit.sh"

mkdir -p "$tempDir/sources/\$OEM\$/\$\$"
mkdir -p "$tempDir/sources/\$OEM\$/\$1"
mkdir -p "$tempDir/sources/\$OEM\$/\$progs"
mkdir -p "$tempDir/sources/\$OEM\$/\$\$/Setup/Chromium"
lndir "$(realpath "targets/main_windefend/resources/\$OEM\$/\$\$")" "$tempDir/sources/\$OEM\$/\$\$"
lndir "$(realpath "targets/main_windefend/resources/\$OEM\$/\$1")" "$tempDir/sources/\$OEM\$/\$1"
lndir "$(realpath "targets/main_windefend/resources/\$OEM\$/\$progs")" "$tempDir/sources/\$OEM\$/\$progs"
wrar_version=$(cat "packages/pkgver/wrar_version.txt")
ln -s "$(realpath "packages/winrar-x64-${wrar_version}es.exe")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/winrar-x64-${wrar_version}es.exe"
chromium_version=$(cat "packages/pkgver/chromium_version.txt")
ln -s "$(realpath "packages/ungoogled-chromium_${chromium_version}_installer_x64.exe")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Scripts_audit/ungoogled-chromium_${chromium_version}-1.1_installer_x64.exe"
ln -s "$(realpath "packages/Chromium.Web.Store.crx")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Chromium/Chromium.Web.Store.crx"
ln -s "$(realpath "packages/uBlock.Origin.crx")" "$tempDir/sources/\$OEM\$/\$\$/Setup/Chromium/uBlock.Origin.crx"

tempDir2=$(basename "$tempDir")
# shellcheck disable=SC2140
wine "packages/oscdimg.exe" -bootdata:2#p0,e,b"Z:\tmp\\$tempDir2\boot\etfsboot.com"#pEF,e,b"Z:\tmp\\$tempDir2\efi\Microsoft\boot\efisys_noprompt.bin" -o -m -u2 -udfver102 -lwin_audit "Z:\\tmp\\$tempDir2" "out\\win_audit.iso"

rm -rf "$tempDir"
