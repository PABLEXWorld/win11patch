#!/bin/bash
set -e
7z x -oout "$dartiso" "$dartmsi"
msiextract "out/$dartmsi" -C "out"
cabextract -d "out/DartTools" "out/$dartmsiout/Toolsx64.cab"
cp "out/$dartmsiout/mfc100u.dll" "out"
cp "out/$dartmsiout/MSDartCmn.dll" "out"
rm -rf "out/DaRT"
rm -rf "out/$dartmsiout"
cp "packages/DartConfig.exe" "out"
pushd out
wine DartConfig.exe
popd
rm "out/mfc100u.dll"
rm "out/MSDartCmn.dll"
rm "out/DartConfig.exe"
