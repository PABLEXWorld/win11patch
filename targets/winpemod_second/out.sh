#!/bin/bash
set -e
rm "out/boot.wim"
mv "out/shared/boot.wim" "out"
mv "out/shared/winre.wim" "out"
rm -rf "out/shared"
rm -rf "out/regmod_winpe"
rm -rf "out/winpemod"
wimlib-imagex optimize "out/boot.wim" --recompress --compress=maximum
wimlib-imagex optimize "out/winre.wim" --recompress --compress=maximum
