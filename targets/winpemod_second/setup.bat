diskpart /s winpescripts\diskpart_script.txt                                        || exit /b 1
set driveletter=C:
set mount=%driveletter%\mount
md %mount%                                                                          || exit /b 1
call :loop boot.wim 1                                                               || exit /b 1
call :loop boot.wim 2                                                               || exit /b 1
call :loop winre.wim 1                                                              || exit /b 1
goto :eof

:loop
dism /Mount-Image /ImageFile:%1 /index:%2 /MountDir:%mount%                         || exit /b 1
del %mount%\Windows\System32\WallpaperHost.exe                                      || exit /b 1
xcopy /E /Y /Z winpemod %mount%                                                     || exit /b 1
reg load HKLM\SOFTWARE2 %mount%\Windows\System32\config\SOFTWARE                    || exit /b 1
reg load HKLM\SYSTEM2 %mount%\Windows\System32\config\SYSTEM                        || exit /b 1
call winpescripts\regmod.bat winpe                                                  || exit /b 1
reg unload HKLM\SOFTWARE2                                                           || exit /b 1
reg unload HKLM\SYSTEM2                                                             || exit /b 1
IF NOT "%1" == "winre.wim" del %mount%\Windows\System32\winpeshl.ini                || exit /b 1
dism /Unmount-Image /MountDir:%mount% /Commit                                       || exit /b 1
