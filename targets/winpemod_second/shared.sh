#!/bin/bash
set -e
source "targets/winpe/shared.sh"
source "tools/winpemod/winpemod_second.sh"
source "targets/winpemod_second/dart.sh"
rsync -r -P --size-only "out/regmod_winpe" "out/shared"
rsync -r -P --size-only "out/winpemod" "out/shared"
rsync -P --size-only "out/boot.wim" "out/shared"
wimlib-imagex extract "out/install.wim" 1 "Windows/System32/Recovery/winre.wim" --dest-dir="out/shared"
rsync -r -P --size-only "out/DartTools/" "out/shared/winpemod"
rsync -P --size-only "out/DartConfig.dat" "out/shared/winpemod/Windows/System32"
rsync -P --size-only "targets/winpemod_second/windows11-bypass.reg" "out/shared/regmod_winpe/windows11-bypass.reg"
rm -rf "out/DartTools"
rm "out/DartConfig.dat"
