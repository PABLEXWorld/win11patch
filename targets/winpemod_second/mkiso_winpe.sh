#!/bin/bash
set -e

tempDir=`mktemp -d`

mkdir "$tempDir/boot" "$tempDir/efi" "$tempDir/sources"
lndir "$(realpath "out/ISODIR/boot")" "$tempDir/boot"
lndir "$(realpath "out/ISODIR/efi")" "$tempDir/efi"
ln -s "$(realpath "out/$1.wim")" "$tempDir/sources/boot.wim"
ln -s "$(realpath "out/ISODIR/bootmgr")" "$tempDir/bootmgr"
ln -s "$(realpath "out/ISODIR/bootmgr.efi")" "$tempDir/bootmgr.efi"

tempDir2=$(basename $tempDir)
# shellcheck disable=SC2140
wine "packages/oscdimg.exe" -bootdata:2#p0,e,b"Z:\tmp\\$tempDir2\boot\etfsboot.com"#pEF,e,b"Z:\tmp\\$tempDir2\efi\Microsoft\boot\efisys_noprompt.bin" -o -m -u2 -udfver102 -l$1 "Z:\\tmp\\$tempDir2" "out\\$1.iso"

rm -rf "$tempDir"
