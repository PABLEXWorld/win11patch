diskpart /s winpescripts\diskpart_script.txt                                        || exit /b 1
set driveletter=C:
md %driveletter%\scratch
robocopy uup-converter-wimlib %driveletter%\uup-converter-wimlib\ /MIR
cd /D %driveletter%\uup-converter-wimlib
md %driveletter%\temp                                                               || exit /b 1
set TEMP=%driveletter%\temp
rd /S /Q %SystemRoot%\temp
call convert-UUP.cmd
Z:
robocopy %driveletter%\uup-converter-wimlib uup-converter-wimlib\ /MIR
exit /b 0
