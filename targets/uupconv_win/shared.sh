#!/bin/bash
set -e
source targets/winpe/shared.sh
rm -rf "out/intermediates"
rsync -r -P --size-only "packages/uup-converter-wimlib" "out/shared"
rsync -r -P --size-only "$UUPs_main" "out/shared/uup-converter-wimlib"
sed -i 's@set _dism1=dism.exe /English@set _dism1=dism.exe /English /ScratchDir:c:\Scratch@g' "out/shared/uup-converter-wimlib/convert-UUP.cmd"
sed -i 's@%SystemRoot%\\temp@C:\\temp@g' "out/shared/uup-converter-wimlib/convert-UUP.cmd"
rsync -P "targets/uupconv_win/ConvertConfig.ini" "out/shared/uup-converter-wimlib"
