#!/bin/bash
set -e
folder=$(basename "$(find "out/shared/uup-converter-wimlib" -maxdepth 1 -type d -not -iname "bin" -not -iname "UUPs" -not -iname "Drivers" | tail -n 1)")
mv "out/shared/uup-converter-wimlib/$folder" "out/ISODIR"
echo "$folder" > "out/isodir.txt"
rm -rf "out/shared"
mv "out/ISODIR/sources/boot.wim" "out"
mv "out/ISODIR/sources/install.wim" "out"
wimlib-imagex extract "out/install.wim" 1 "\Program Files (x86)\Microsoft\Edge\Application\*\Installer\*" --dest-dir="out/Edge"
rm "out/ISODIR/boot/bootfix.bin" "out/ISODIR/sources/appraiserres.dll"
# rm "out/ISODIR/__chunk_data"
# rm -r "out/ISODIR/sources/_manifest"
rm -r "out/ISODIR/sources/uup"
touch "out/ISODIR/sources/appraiserres.dll"
