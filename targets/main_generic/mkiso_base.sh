#!/bin/bash
set -e

tempDir=`mktemp -d`

lndir "$(realpath "out/ISODIR")" "$tempDir"
ln -s "$(realpath "targets/main_generic/resources/ei.cfg")" "$tempDir/sources"
ln -s "$(realpath "targets/main_generic/resources/pid.txt")" "$tempDir/sources"
ln -s "$(realpath "out/boot.wim")" "$tempDir/sources/boot.wim"
