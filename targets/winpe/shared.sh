#!/bin/bash
set -e
mkdir -p "out/shared"
rsync -r -P --size-only "targets/winpe/winpescripts" "out/shared"
