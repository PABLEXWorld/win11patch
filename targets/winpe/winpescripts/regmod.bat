setlocal enabledelayedexpansion
for /f "tokens=1 delims=" %%i in ('dir /S /B regmod_%1') do (
    if %%~xi==.reg (
        set filename=%%~ni
        if not "!filename:~0,1!"=="#" (
            echo %%i
            reg import "%%i" || exit /b 1
        )
    )
)
endlocal
