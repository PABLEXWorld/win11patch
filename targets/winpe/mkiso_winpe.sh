#!/bin/bash
set -e

tempDir=`mktemp -d`

lndir "$(realpath "out/intermediates/ISODIR")" "$tempDir"
mkdir "$tempDir/sources"
ln -s "$(realpath "out/$1.wim")" "$tempDir/sources/boot.wim"

tempDir2=${tempDir//\//\\}
# shellcheck disable=SC2140
wine "packages/oscdimg.exe" -bootdata:2#p0,e,b"Z:$tempDir2\boot\etfsboot.com"#pEF,e,b"Z:$tempDir2\efi\Microsoft\boot\efisys_noprompt.bin" -o -m -u2 -udfver102 -l$1 "Z:$tempDir2" "out\\$1.iso"

rm -rf "$tempDir"
rm "out/$1.wim"
