@echo off
REM drvload X:\drivers\qxldod\qxldod.inf
wpeinit
net use Z: \\10.0.2.4\qemu > nul            || pause && exit
Z:                                          || pause && exit
xcopy /E winpescripts\* X:                  || pause && exit
del mount                                   || pause && exit
copy setup.cmd X:\setup.cmd                 || pause && exit
del setup.cmd                               || pause && exit
call X:\setup.cmd                           || pause && exit
echo. > setupdone                           || pause && exit
X:                                          || pause && exit
net use Z: /D > nul                         || pause && exit
wpeutil shutdown
