#!/bin/bash
set -e
tempDir=$(mktemp -d)

touch "out/shared/mount"

if [ -f "out/shared/setup.cmd" ]; then rm "out/shared/setup.cmd"; fi
cp "targets/$1/setup.bat" "out/shared/setup.cmd"
unix2dos "out/shared/setup.cmd"

qemu-img create -f qcow2 "$tempDir/hd.qcow2" 64G
set +e
# shellcheck disable=SC2046
cmd=$(qemu-system-x86_64 \
-d guest_errors \
-machine q35 \
-accel kvm \
-cpu host,hv_crash \
-smp $(nproc) \
-m 4G \
-cdrom "out/winpe.iso" \
-drive id=disk,file="$tempDir/hd.qcow2",if=none,format=qcow2 \
-device ahci,id=ahci \
-device ide-hd,drive=disk,bus=ahci.0 \
-nic user,smb=$(realpath "out/shared"),restrict=on \
-vga qxl \
-usb \
-device usb-kbd \
-device usb-tablet \
-no-reboot 2>&1)
# shellcheck disable=SC2181
if [ ! "$?" -eq 0 ]; then
  echo QEMU has errored.
  rm -rf "$tempDir"
  exit 1
fi
set -e
if echo "$cmd" | grep -q 'Guest crashed'; then
  if [ -f "out/shared/mount" ]; then rm "out/shared/mount"; fi
  echo The virtual machine has BSOD\'d.
  rm -rf "$tempDir"
  exit 1
fi

error=0
if [ -f "out/shared/mount" ]; then rm "out/shared/mount"; error="1"; fi
if [ ! -f "out/shared/setupdone" ]; then error="1"; fi

if [ "$error" -eq 1 ]; then
  echo An error occured with the virtual machine.
  rm -rf "$tempDir"
  exit 1
fi

rm "out/shared/setupdone"
rm -rf "$tempDir"
