#!/bin/bash
set -e

find tools/buildpackages -maxdepth 1 -type d -not -iname "buildpackages" -not -iname "TestRegistryChanges" | parallel -j+0 {}/build.sh
