#!/bin/bash
set -e

if [ ! -f "hd.qcow2" ]; then qemu-img create -f qcow2 "hd.qcow2" 64G; fi
set +e
# shellcheck disable=SC2046
cmd=$(qemu-system-x86_64 \
-d guest_errors \
-machine q35 \
-accel kvm \
-cpu host,hv_crash \
-smp $(nproc) \
-m 4G \
-cdrom "out/win_audit.iso" \
-drive id=disk,file="hd.qcow2",if=none,format=qcow2 \
-device ahci,id=ahci \
-device ide-hd,drive=disk,bus=ahci.0 \
-nic none \
-vga qxl \
-usb \
-device usb-kbd \
-device usb-tablet 2>&1)
# shellcheck disable=SC2181
if [ ! "$?" -eq 0 ]; then
  echo QEMU has errored.
  exit 1
fi
set -e
if echo "$cmd" | grep -q 'Guest crashed'; then
  echo The virtual machine has BSOD\'d.
  exit 1
fi
