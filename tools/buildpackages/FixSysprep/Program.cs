﻿using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;

namespace FixSysprep
{
    static class Program
    {
        const string slash = @"\";
        const string mountdir = "C:"+slash+"mount";
        const string windir = mountdir+slash+"Windows";
        const string system32 = windir+slash+"System32";
        const string dir = "ActionFiles";
        const string inputDir = system32+slash+"Sysprep"+slash+dir;

        static void AddSpModules(ref XElement sysprepInfo, XElement root) {

            foreach (XElement elem in root.Elements())
            {
                IEnumerable<string> moduleNames = GetSpModuleNames(elem);
                
                if (!moduleNames.Any()) {
                    sysprepInfo.Add(elem);
                    continue;
                }

                moduleNames = moduleNames.Select(x => ResolveSpModuleName(x));

                if (!moduleNames.Any(x => !File.Exists(x)))
                    sysprepInfo.Add(elem);
            }
        }

        static IEnumerable<string> GetSpModuleNames(XElement elem)
        {
            IEnumerable<XElement> sysprepModule = elem.Elements().Where(x => x.Name == "sysprepModule");
            return sysprepModule?.Attributes().Where(x => x.Name == "moduleName").Select(x => x.Value);
        }

        static string ResolveSpModuleName(string moduleName)
        {
            string result = moduleName.Replace("$(runtime.windows)", windir);
            result = result.Replace("$(runtime.system32)", system32);

            if (!result.Contains('\\'))
                result = system32+slash+moduleName;

            return result;
        }

        static void Main(string[] args)
        {
            if (Directory.Exists(dir)) Directory.Delete(dir, true);

            foreach (string actionfile in Directory.EnumerateFiles(inputDir))
            {
                XDocument newdoc = new XDocument();
                XElement sysprepInfo = new XElement("sysprepInformation");

                XElement root = XDocument.Load(actionfile).Root;
    
                AddSpModules(ref sysprepInfo, root);

                newdoc.Add(sysprepInfo);

                Directory.CreateDirectory(dir);

                newdoc.Save(Path.Combine(dir, Path.GetFileName(actionfile)));
            }
        }
    }
}
