#!/bin/bash
set -e

pushd "tools/buildpackages/FixSysprep"
    msbuild -p:Configuration="Release"
popd

cp "tools/buildpackages/FixSysprep/bin/Release/FixSysprep.exe" "packages"

pushd "tools/buildpackages/FixSysprep"
    msbuild -p:Configuration="Release" -t:clean
    rm -r "bin"
    rm -r "obj"
popd
