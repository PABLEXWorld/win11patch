﻿using System;
using System.IO;
using Microsoft.Win32;

namespace DeleteTasks
{
    using static TaskHandling;
    using static ExceptionHandling;

    static class Program
    {
        static RegistryKey tasksCache = TryOpenRegSubKey(Registry.LocalMachine, @"SOFTWARE2\Microsoft\Windows NT\CurrentVersion\Schedule\TaskCache");
        static RegistryKey tasksCache_tasks = TryOpenRegSubKey(tasksCache, "Tasks");
        static RegistryKey tasksCache_tree = TryOpenRegSubKey(tasksCache, "Tree");

        static RegistryKey[] tasksCache_types = {
            TryOpenRegSubKey(tasksCache, "Boot"),
            TryOpenRegSubKey(tasksCache, "Logon"),
            TryOpenRegSubKey(tasksCache, "Maintenance"),
            TryOpenRegSubKey(tasksCache, "Plain") };

        static string[] tasksCache_taskuuidlist = tasksCache_tasks.GetSubKeyNames();

        /// <summary>
        /// Attempts to read task folder removal list from disk.
        /// Prints and throws exception if the file couldn't be read.
        /// </summary>
        static string[] TryReadTaskRemovallist()
        {
            try { return File.ReadAllLines("taskremovelist.txt"); }
            catch (Exception e) { throw PrintException(e, "Error reading taskremovelist.txt"); }
        }

        static string[] removelist = TryReadTaskRemovallist();

        /// <summary>
        /// Removes entire preprovisioned task folders from an offline Windows image.
        /// Prints and throws exception if it exists and couldn't be deleted.
        /// </summary>
        static void DeleteOffScheduledTaskFolder(string taskfolderpath)
        {
            // Recursively delete task folder in filesystem.
            TryDeleteTaskDirectory(taskfolderpath);

            // Recursively delete task folder in Tree registry key.
            TryDeleteRegSubKey(tasksCache_tree, taskfolderpath.Substring(1));

            foreach (string tasksCache_taskuuid in tasksCache_taskuuidlist)
            {
                RegistryKey tasksCache_task = tasksCache_tasks.OpenSubKey(tasksCache_taskuuid);

                // if registry key does not exist, skip. Should not normally occur since we just probed the list of installed task UUIDs.
                if (tasksCache_task == null)
                    continue;

                string tasksCache_task_path = (string)tasksCache_task.GetValue("Path");

                tasksCache_task.Close();

                // Check if scheduled task is contained in the folder we want to delete.
                if (tasksCache_task_path == null || !tasksCache_task_path.StartsWith(taskfolderpath))
                    continue;

                // Recursively delete task UUID keys that match our description.
                TryDeleteRegSubKey(tasksCache_tasks, tasksCache_taskuuid);

                // Recursively delete task UUID keys that match our description from event task lists.
                foreach (RegistryKey tasksCache_type in tasksCache_types)
                    TryDeleteRegSubKey(tasksCache_type, tasksCache_taskuuid);
            }
        }

        static void Main(string[] args)
        {
            foreach (string taskfolderpath in removelist)
            {
                // skip commented out lines
                if (taskfolderpath[0] == '#') continue;

                DeleteOffScheduledTaskFolder(taskfolderpath);
            }
        }
    }
}
