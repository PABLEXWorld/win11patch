using System;
using System.IO;
using Microsoft.Win32;
using System.Security.AccessControl;

namespace DeleteTasks
{
    using static ExceptionHandling;
    static class TaskHandling
    {
        /// <summary>
        /// Checks if a given task folder exists in filesystem, and deletes it if so.
        /// Prints and throws exception if it exists and couldn't be deleted.
        /// </summary>
        internal static void TryDeleteTaskDirectory(string taskfolderpath)
        {
            if (!Directory.Exists("C:\\mount\\Windows\\System32\\Tasks" + taskfolderpath))
                return;

            try { Directory.Delete("C:\\mount\\Windows\\System32\\Tasks" + taskfolderpath, true); }
            catch (Exception e) { throw PrintException(e, $"Error deleting task directory C:\\mount\\Windows\\System32\\Tasks{taskfolderpath}"); }
        }

        /// <summary>
        /// Attempts to open a registry subkey for access.
        /// Prints and throws exception if it is missing or we'reunable to access.
        /// </summary>
        internal static RegistryKey TryOpenRegSubKey(RegistryKey key, string path)
        {
            try
            {
                RegistryKey sk = key.OpenSubKey(path, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl);
                if (sk == null)
                    throw new Exception("Registry key not found.");
                return sk;
            }

            catch (Exception e) { throw PrintException(e, $"Error opening essential target registry key HKLM\\{path}"); }
        }

        /// <summary>
        /// Checks if a subkey exists, and deletes it if so.
        /// Prints and throws exception if it exists and couldn't be deleted.
        /// </summary>
        internal static void TryDeleteRegSubKey(RegistryKey key, string subkey)
        {
            RegistryKey sk = key.OpenSubKey(subkey);
            if (sk == null)
                return;
            sk.Close();

            try { key.DeleteSubKeyTree(subkey); }
            catch (Exception e) { throw PrintException(e, $"Error deleting task subkey {key.Name}{subkey}"); }
        }
    }
}