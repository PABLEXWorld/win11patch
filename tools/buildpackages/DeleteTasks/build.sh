#!/bin/bash
set -e

pushd "tools/buildpackages/DeleteTasks"
    msbuild -p:Configuration="Release"
popd

cp "tools/buildpackages/DeleteTasks/bin/Release/DeleteTasks.exe" "packages"

pushd "tools/buildpackages/DeleteTasks"
    msbuild -p:Configuration="Release" -t:clean
    rm -r "bin"
    rm -r "obj"
popd
