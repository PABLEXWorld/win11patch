using System;
using static System.Console;

namespace DeleteTasks
{
    static class ExceptionHandling
    {
        /// <summary>
        /// Prints basic exception information with a custom message, then returns input exception to be rethrown.
        /// </summary>
        internal static Exception PrintException(Exception e, string custMsg)
        {
            Error.WriteLine($"{e.GetType().Name}; {custMsg}; {e.Message}");
            return e;
        }
    }
}