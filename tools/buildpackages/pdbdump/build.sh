#!/bin/bash
set -e

source "tools/buildpackages/pdbdump/gatherfiles.sh"

pushd "tools/buildpackages/pdbdump"
    if [ ! -d "bin/Release" ]; then mkdir -p "bin/Release"; fi
    x86_64-w64-mingw32-gcc -L. -ldbghelp pdbdump.c -o bin/Release/pdbdump.exe
popd

cp "tools/buildpackages/pdbdump/bin/Release/pdbdump.exe" "packages"

pushd "tools/buildpackages/pdbdump"
    rm "dbghelp.dll"
    rm -r "bin"
popd
