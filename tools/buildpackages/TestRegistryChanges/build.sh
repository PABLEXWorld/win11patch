#!/bin/bash
set -e

pushd "tools/buildpackages/TestRegistryChanges"
    msbuild -p:Configuration="Release"
popd

cp "tools/buildpackages/TestRegistryChanges/bin/Release/TestRegistryChanges.exe" "packages"

pushd "tools/buildpackages/TestRegistryChanges"
    msbuild -p:Configuration="Release" -t:clean
    rm -r "bin"
    rm -r "obj"
popd
