#!/bin/bash
set -e

source "tools/buildpackages/DartConfig/gatherfiles.sh"

pushd "tools/buildpackages/DartConfig"
    if [ ! -d "bin/Release" ]; then mkdir -p "bin/Release"; fi
    x86_64-w64-mingw32-gcc -L. -lMSDartCmn -lkernel32 DartConfig.c -o bin/Release/DartConfig.exe
popd

cp "tools/buildpackages/DartConfig/bin/Release/DartConfig.exe" "packages"

pushd "tools/buildpackages/DartConfig"
    rm "kernel32.dll"
    rm "MSDartCmn.dll"
    rm -r "bin"
popd
