#!/bin/bash
set -e
cp "$WINEPREFIX/drive_c/windows/system32/kernel32.dll" "tools/buildpackages/DartConfig"
7z x -oout "$dartiso" "$dartmsi"
msiextract "out/$dartmsi" -C "out"
rm -rf "out/DaRT"
cp "out/$dartmsiout/MSDartCmn.dll" "tools/buildpackages/DartConfig"
rm -rf "out/$dartmsiout"
