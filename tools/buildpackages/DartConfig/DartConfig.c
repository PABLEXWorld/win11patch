#include <windows.h>

extern void SaveDartConfigFileWrapper(LPCWSTR fileName);
extern void SetDartConfigSettingWrapper(unsigned int settingID, unsigned int flags, LPCWSTR data);
extern void ResetDartConfigSettings();

int main(int argc, char** argv)
{
    ResetDartConfigSettings();
    SetDartConfigSettingWrapper(1600, 3, NULL);
    SetDartConfigSettingWrapper(700, 2, NULL);
    SetDartConfigSettingWrapper(300, 2, NULL);
    SetDartConfigSettingWrapper(500, 2, NULL);
    SetDartConfigSettingWrapper(600, 2, NULL);
    SetDartConfigSettingWrapper(800, 2, NULL);
    SetDartConfigSettingWrapper(400, 2, NULL);
    SetDartConfigSettingWrapper(1100, 2, NULL);
    SetDartConfigSettingWrapper(200, 2, NULL);
    SetDartConfigSettingWrapper(100, 2, NULL);
    SetDartConfigSettingWrapper(1500, 2, NULL);
    SetDartConfigSettingWrapper(1200, 2, NULL);
    SetDartConfigSettingWrapper(900, 2, NULL);
    SetDartConfigSettingWrapper(1000, 2, NULL);
    SetDartConfigSettingWrapper(1300, 2, NULL);
    SetDartConfigSettingWrapper(700, 3, NULL);
    SetDartConfigSettingWrapper(300, 3, NULL);
    SetDartConfigSettingWrapper(500, 3, NULL);
    SetDartConfigSettingWrapper(600, 3, NULL);
    SetDartConfigSettingWrapper(800, 3, NULL);
    SetDartConfigSettingWrapper(400, 3, NULL);
    SetDartConfigSettingWrapper(1100, 3, NULL);
    SetDartConfigSettingWrapper(200, 3, NULL);
    SetDartConfigSettingWrapper(100, 3, NULL);
    SetDartConfigSettingWrapper(1500, 3, NULL);
    SetDartConfigSettingWrapper(1520, 0, L"3388");
    SetDartConfigSettingWrapper(1510, 0, L"");
    SetDartConfigSettingWrapper(1200, 3, NULL);
    SetDartConfigSettingWrapper(900, 3, NULL);
    SetDartConfigSettingWrapper(1000, 3, NULL);
    SetDartConfigSettingWrapper(1300, 3, NULL);
    SaveDartConfigFileWrapper(L"DartConfig.dat");
    return 0;
}
