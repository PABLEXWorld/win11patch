#!/bin/bash
source tools/uupconv/uupconv_getfirstmeta.sh
set -e
IFS=$'\n'
export WIMLIB_IMAGEX_IGNORE_CASE=1

export_keys () {
  local count=1

  if [ "$1" == "post_sxs" ]; then cmd=$tempDir/sxs_\*.txt
  else cmd=tools/winpemod/resources/\*reg_$1\*.txt; fi

  # shellcheck disable=SC2013,SC2086
  for regkey in $(cat $cmd)
  do
    if [ "$1" == "sxs" ]; then grep $regkey $tempDir/sxs.txt > $tempDir/sxs_$count.txt
    elif [ "$1" == "post_sxs" ]; then reged -x $tempDir/SOFTWARE HKEY_LOCAL_MACHINE\\SOFTWARE2 Microsoft\\Windows\\CurrentVersion\\SideBySide\\Winners\\$regkey out/regmod_winpe/reg_sxs_$count.reg
    else reged -x $tempDir/${1^^} HKEY_LOCAL_MACHINE\\${1^^}2 "$regkey" out/regmod_winpe/reg_$1_$count.reg; fi
    count=$((count+1))
  done
}

extractDir="$tempDir/extract"

cabextractVersion=$(cabextract --version | cut -d ' ' -f 3)
if [ $(version $cabextractVersion) -ge $(version "1.10") ] ; then
  keepSymlinks="-k"
else
  keepSymlinks=""
fi
mkdir -p "$tempDir/UUPs_esd"

# "$UUPs_main/Microsoft-Windows-VBSCRIPT-FoD-Package-wow64.cab"
for file in "$UUPs_main/Microsoft-Windows-MediaPlayer-Package-wow64.cab" "$UUPs_main/Microsoft-Windows-PowerShell-ISE-FOD-Package-amd64.cab" "$UUPs_main/Microsoft-Windows-WMIC-FoD-Package-wow64.cab"; do
  fileName=`basename $file .cab`
  echo -e "$infoColor""CAB -> ESD:""$resetColor"" $fileName"

  mkdir "$extractDir"
  cabextract $keepSymlinks -d "$extractDir" "$file" >/dev/null 2>/dev/null
  errorHandler $? "Failed to extract $fileName.cab"

  wimlib-imagex capture "$extractDir" "$tempDir/UUPs_esd/$fileName.esd" \
    --no-acls --norpfix "Edition Package" "Edition Package" >/dev/null

  errorHandler $? "Failed to create $fileName.esd"

  rm -rf "$extractDir"
done

options=( --no-acls --nullglob --ref={"$UUPs_main","$tempDir/UUPs_esd"}/*.[eE][sS][dD] )

wimlib-imagex extract "$firstMetadata" 3 @"tools/winpemod/resources/dotnet_files.txt" "${options[@]}" --dest-dir="out/winpemod"
wimlib-imagex extract "$firstMetadata" 3 @"tools/winpemod/resources/extra_cmds.txt" "${options[@]}" --dest-dir="out/winpemod"
wimlib-imagex extract "$firstMetadata" 3 @"tools/winpemod/resources/wow64_files.txt" "${options[@]}" --dest-dir="out/winpemod"

mkdir -p "out/regmod_winpe"
wimlib-imagex extract "$firstMetadata" 3 "/Windows/System32/config/SOFTWARE" --no-acls --nullglob --dest-dir="$tempDir"
wimlib-imagex extract "$firstMetadata" 3 "/Windows/System32/config/SYSTEM" --no-acls --nullglob --dest-dir="$tempDir"

export_keys "software"
export_keys "system"

echo "ls Microsoft\\Windows\\CurrentVersion\\SideBySide\\Winners
q" | reged -e "$tempDir/SOFTWARE" | grep '^  <.*>$' - | awk '{ print substr($0, 4, length($0)-4) }' > "$tempDir/sxs.txt"

export_keys "sxs"
export_keys "post_sxs"

cleanup
