#!/bin/bash
set -e
IFS=$'\n'
export WIMLIB_IMAGEX_IGNORE_CASE=1
tempDir=$(mktemp -d)

export_keys () {
  local count=1

  if [ "$1" == "post_sxs" ]; then cmd=$tempDir/sxs_\*.txt
  else cmd=tools/winpemod/resources/\*reg_$1\*.txt; fi

  # shellcheck disable=SC2013,SC2086
  for regkey in $(cat $cmd)
  do
    if [ "$1" == "sxs" ]; then grep $regkey $tempDir/sxs.txt > $tempDir/sxs_$count.txt
    elif [ "$1" == "post_sxs" ]; then reged -x $tempDir/SOFTWARE HKEY_LOCAL_MACHINE\\SOFTWARE2 Microsoft\\Windows\\CurrentVersion\\SideBySide\\Winners\\$regkey out/regmod_winpe/reg_sxs_$count.reg
    else reged -x $tempDir/${1^^} HKEY_LOCAL_MACHINE\\${1^^}2 "$regkey" out/regmod_winpe/reg_$1_$count.reg; fi
    count=$((count+1))
  done
}

wimlib-imagex extract "out/install.wim" 1 @"tools/winpemod/resources/extra_cmds.txt" --no-acls --nullglob --dest-dir="out/winpemod"
wimlib-imagex extract "out/install.wim" 1 @"tools/winpemod/resources/wow64_files.txt" --no-acls --nullglob --dest-dir="out/winpemod"
wimlib-imagex extract "out/install.wim" 1 @"tools/winpemod/resources/dotnet_files.txt" --no-acls --nullglob --dest-dir="out/winpemod"

mkdir -p "out/regmod_winpe"
wimlib-imagex extract "out/install.wim" 1 "\Windows\System32\config\SOFTWARE" --no-acls --nullglob --dest-dir="$tempDir"
wimlib-imagex extract "out/install.wim" 1 "\Windows\System32\config\SYSTEM" --no-acls --nullglob --dest-dir="$tempDir"

export_keys "software"
export_keys "system"

echo "ls Microsoft\\Windows\\CurrentVersion\\SideBySide\\Winners
q" | reged -e "$tempDir/SOFTWARE" | grep '^  <.*>$' - | awk '{ print substr($0, 4, length($0)-4) }' > "$tempDir/sxs.txt"

export_keys "sxs"
export_keys "post_sxs"

rm -rf "$tempDir"
