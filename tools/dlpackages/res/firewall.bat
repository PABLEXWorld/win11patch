@echo off

set "SingleNul=>nul"
set "MultiNul=1>nul 2>&1"

%MultiNul% fltmc || (
	set "_=call "%~dpfx0" %*" & powershell -nop -c start cmd -args '/d/x/r',$env:_ -verb runas || (
	>"%temp%\Elevate.vbs" echo CreateObject^("Shell.Application"^).ShellExecute "%~dpfx0", "%*" , "", "runas", 1
	%SingleNul% "%temp%\Elevate.vbs" & del /q "%temp%\Elevate.vbs" )
	exit)

PowerShell.exe -ExecutionPolicy Bypass -Command "& {foreach ($file in Get-ChildItem -Recurse \"C:\Program Files\Common Files\microsoft shared\ClickToRun\" -include *.exe) {New-NetFirewallRule -Program \"$file\" -DisplayName \"$file\" -Profile @('Domain', 'Private', 'Public') -Direction Outbound -Action Block}}" || pause

PowerShell.exe -ExecutionPolicy Bypass -Command "& {foreach ($file in Get-ChildItem -Recurse \"C:\Program Files\Common Files\microsoft shared\OFFICE16\" -include *.exe) {New-NetFirewallRule -Program \"$file\" -DisplayName \"$file\" -Profile @('Domain', 'Private', 'Public') -Direction Outbound -Action Block}}" || pause
