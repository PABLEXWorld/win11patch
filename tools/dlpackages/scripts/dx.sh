# DirectX End-User Runtimes

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" dx

source "$(dirname "${BASH_SOURCE[0]}")/utils/get-microsoft.sh" 8109 "$version" > "$verfile"

if [ -f "packages/$name" ]; then
    if [ -d packages/DX ]; then rm -r packages/DX; fi
    wine "packages/$name" /T:Z:DX /C /Q
    mv DX/DX "packages/DX"
    rmdir DX
    rm "packages/$name"
fi
