# Office Deployment Tool

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" odt

source "$(dirname "${BASH_SOURCE[0]}")/utils/get-microsoft.sh" 49117 "$version" > "$verfile"

if [ -n "$version" ] && [ ! "$name" == "$version" ] && [ -d "packages/${version%.exe}" ]; then
    rm -r "packages/${version%.exe}"
fi

if [ ! -d "packages/${name%.exe}" ]; then
    wine "packages/$name" /extract:"packages/${name%.exe}" /quiet
    rm "packages/$name"
fi
