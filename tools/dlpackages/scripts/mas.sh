# KMS-VL-ALL-AIO

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" mas

name=$(curl "https://api.github.com/repos/massgravel/Microsoft-Activation-Scripts/commits" | jq -r ".[0].sha")

fname=$(curl "https://api.github.com/repos/massgravel/Microsoft-Activation-Scripts/contents/MAS/All-In-One-Version" | jq -r ".[0].name")

if [ "" == "$version" ] || [ ! "$name" == "$version" ]; then
    if [ -f packages/MAS_AIO.cmd ]; then rm packages/MAS_AIO.cmd; fi
    wget --directory-prefix=packages -O packages/MAS_AIO.cmd "https://github.com/massgravel/Microsoft-Activation-Scripts/raw/master/MAS/All-In-One-Version/$fname"
    # sed -i 's/set uAutoRenewal=0/set uAutoRenewal=1/g' packages/KMS_VL_ALL_AIO.cmd
    # sed -i 's/set Silent=0/set Silent=1/g' packages/KMS_VL_ALL_AIO.cmd
fi

echo "$name" > "$verfile"
