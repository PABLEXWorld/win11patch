# WinRAR

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" wrar

link=$(curl "https://www.rarlab.com/download.htm")
link="https://www.rarlab.com$(echo "$link" | xmllint --html --xpath 'string(//*[contains(text(),"Localized WinRAR versions")]/following::table//*[contains(text(),"Spanish (64 bit)")]/../@href)' - 2> /dev/null)"
name=$(basename "$link")
name=${name:11:-6}

if [ "" == "$version" ] || [ ! "$name" == "$version" ]; then
    wget --directory-prefix=packages "$link"
    if [ -f "packages/winrar-x64-${version}es.exe" ]; then rm "packages/winrar-x64-${version}es.exe"; fi
fi

echo "$name" > "$verfile"
