# Oscdimg

set -e

if [ ! -f packages/oscdimg.exe ]; then
    link=$(curl "https://learn.microsoft.com/en-us/windows-hardware/get-started/adk-install" | tidy -q --custom-tags empty --output-xml true 2>/dev/null | xmllint --xpath 'string(//*[contains(text()[1],"Download the Windows ADK")]/@href)' -)
    wget -O packages/adksetup.exe "$link"
    source "$(dirname "${BASH_SOURCE[0]}")/utils/get-github.sh" wixtoolset/wix3 0 ""
    unzip "packages/$filename" dark.exe wix.dll winterop.dll -d packages

    wine packages/dark.exe packages/adksetup.exe -x packages
    rm packages/adksetup.exe
    rm packages/dark.exe
    rm packages/wix.dll
    rm packages/winterop.dll
    rm "packages/$filename"
    dos2unix packages/UX/BootstrapperApplicationData.xml
    sed -i 's/<BootstrapperApplicationData xmlns="http:\/\/schemas.microsoft.com\/wix\/2010\/BootstrapperApplicationData">/<BootstrapperApplicationData>/g' packages/UX/BootstrapperApplicationData.xml
    sed -i 's/<?xml version="1.0" encoding="utf-16"?>/<?xml version="1.0" encoding="utf-8"?>/g' packages/UX/BootstrapperApplicationData.xml
    dos2unix packages/UX/UserExperienceManifest.xml
    sed -i 's/<UserExperienceManifest xmlns="http:\/\/schemas.microsoft.com\/Setup\/2010\/01\/Burn\/UserExperience">/<UserExperienceManifest>/g' packages/UX/UserExperienceManifest.xml
    count=$(xmllint --xpath 'count(//WixPayloadProperties[@Package="package_OscdimgDesktopEditions_x86_en_us"])' packages/UX/BootstrapperApplicationData.xml)
    dlroot=$(xmllint --xpath 'string(/UserExperienceManifest/Settings/SourceResolution/DownloadRoot/text())' packages/UX/UserExperienceManifest.xml)
    dlroot=$(curl -s -w "%{redirect_url}" "$dlroot")
    if [ ! -d packages/oscdimg ]; then mkdir packages/oscdimg; fi

    for ((i=1; i <= count; i++))
    do
        file=$(xmllint --xpath "string(//WixPayloadProperties[@Package=\"package_OscdimgDesktopEditions_x86_en_us\"][$i]/@Name)" packages/UX/BootstrapperApplicationData.xml)
        file=${file/\\/\/}
        name=$(basename "$file")
        wget --output-document="packages/oscdimg/$name" "$dlroot/$file"
    done

    msiextract "packages/oscdimg/Oscdimg (DesktopEditions)-x86_en-us.msi" -C packages/oscdimg
    cp "packages/oscdimg/Program Files/Windows Kits/10/Assessment and Deployment Kit/Deployment Tools/x86/Oscdimg/oscdimg.exe" packages
    rm -r packages/UX
    rm -r packages/oscdimg
fi
