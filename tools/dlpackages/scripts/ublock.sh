#!/bin/bash
# uBlock Origin

set -e

relinfo=$(curl -s "https://api.github.com/repos/ungoogled-software/ungoogled-chromium-windows/releases/latest")
name=$(echo "$relinfo" | jq -r ".tag_name")

IFS='-' read -ra splitver <<< "$name"
chromiumversion=${splitver[0]}

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" ublock

name=$(curl "https://clients2.google.com/service/update2/crx?response=updatecheck&acceptformat=crx2,crx3&prodversion=$chromiumversion&x=id%3Dcjpalhdlnbpafiamejdnhcphjbkeiagm%26uc")
name=$(echo "$name" | sed 's/gupdate xmlns="http:\/\/www.google.com\/update2\/response"/gupdate/g' | xmllint --xpath "string(//updatecheck/@version)" -)

if [ "" == "$version" ] || [ ! "$name" == "$version" ]; then
    if [ -f packages/uBlock.Origin.crx ]; then rm packages/uBlock.Origin.crx; fi
    wget -O packages/uBlock.Origin.crx "https://clients2.google.com/service/update2/crx?response=redirect&acceptformat=crx2,crx3&prodversion=$chromiumversion&x=id%3Dcjpalhdlnbpafiamejdnhcphjbkeiagm%26installsource%3Dondemand%26uc"
fi
echo "$name" > "$verfile"
