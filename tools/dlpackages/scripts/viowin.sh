# VirtIO QXL Driver

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" viowin

link=$(curl -s -w "%{redirect_url}" -o /dev/null "https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio")
name=$(curl -L "$link" | xmllint --html --xpath 'string(//a[contains(text(),".iso")]/@href)' - 2> /dev/null)

if [ "" == "$version" ] || [ ! "$name" == "$version" ]; then
    if [ -d packages/qxldod ]; then rm -rf packages/qxldod; fi

    wget --directory-prefix=packages "$link/$name"
    7z e -aoa -opackages/qxldod "packages/$name" qxldod/w10/amd64/*
    rm "packages/$name"
fi

echo "$name" > "$verfile"
