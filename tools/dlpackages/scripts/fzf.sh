# ExplorerPatcher

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" fzf

source "$(dirname "${BASH_SOURCE[0]}")/utils/get-github.sh" junegunn/fzf 12 "$version"

echo "$newver" > "$verfile"

if [ -f "packages/$filename" ]; then
    if [ -f packages/fzf.exe ]; then rm packages/fzf.exe; fi
    unzip -o "packages/$filename" -d "packages"
    rm "packages/$filename"
fi
