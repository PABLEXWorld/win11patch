# PowerToys

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" ptoys
version="v$version"
source "$(dirname "${BASH_SOURCE[0]}")/utils/fname.sh" ptoys

source "$(dirname "${BASH_SOURCE[0]}")/utils/get-github.sh" microsoft/PowerToys 5 "$version"
if [ -n "$filename_old" ] && [ ! "$filename" == "$filename_old" ]; then rm "packages/$filename_old"; fi
newver=${newver:1}

echo "$newver" > "$verfile"
echo "$filename" > "$oldfnfile"
