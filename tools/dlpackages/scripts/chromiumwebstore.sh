# Chromium Web Store

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" chromiumwebstore

source "$(dirname "${BASH_SOURCE[0]}")/utils/get-github.sh" NeverDecaf/chromium-web-store 0 "v$version"
newver=${newver:1}

echo "$newver" > "$verfile"
