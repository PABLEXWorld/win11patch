# Sticky Notes

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" sticky

dlroot="https://win7games.com"
file=$(curl "$dlroot" | xmllint --html --xpath "string(//*[@id='sticky']//*[@class='button button-primary download']/@href)" - 2> /dev/null)
name=$(basename "$file")

if [ "" == "$version" ] || [ ! "$name" == "$version" ]; then
    wget --directory-prefix=packages --header="Referer: https://win7games.com/" "$dlroot$file"
    unzip -o "packages/$name" -d "packages"
    rm "packages/$name"
fi

echo "$name" > "$verfile"
