#Visual C++ Redistributable

set -e

if [ -f packages/vcredist.x64.exe ]; then rm packages/vcredist.x64.exe; fi
link=$(curl -L "https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist")
link=$(echo "$link" | xmllint --html --xpath 'string(//*[@id="microsoft-visual-c-redistributable-latest-supported-downloads"]/following::table//*[contains(text(),"vc_redist.x64")]/@href)' - 2> /dev/null)

wget --directory-prefix=packages "$link"
