# Librewolf

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" librewolf
source "$(dirname "${BASH_SOURCE[0]}")/utils/fname.sh" librewolf

source "$(dirname "${BASH_SOURCE[0]}")/utils/get-gitlab.sh" 44042130 ^librewolf-.+-windows-x86_64-setup\.exe$ "$version"

echo "$newver" > "$verfile"
echo "$filename" > "$oldfnfile"
