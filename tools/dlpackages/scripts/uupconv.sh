# Multiplatform UUP converter

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" uupconv

name=$(curl "https://git.uupdump.net/api/v1/repos/uup-dump/converter/commits" | jq -r ".[0].sha")

if [ "" == "$version" ] || [ ! "$name" == "$version" ]; then
    if [ -d packages/uupconv ]; then rm -r packages/uupconv; fi
    wget --output-document=packages/uupconv.zip "https://git.uupdump.net/uup-dump/converter/archive/master.zip"
    unzip -jo packages/uupconv.zip -d packages/uupconv
    rm packages/uupconv.zip
fi

echo "$name" > "$verfile"
