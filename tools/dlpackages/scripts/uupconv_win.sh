# Multiplatform UUP converter

set -e


source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" uupconv_win

name=$(curl "https://git.uupdump.net/api/v1/repos/uup-dump/misc/commits" | jq -r ".[0].sha")

if [ "" == "$version" ] || [ ! "$name" == "$version" ]; then
    if [ -d packages/uup-converter-wimlib ]; then rm -r packages/uup-converter-wimlib; fi
    wget --output-document=packages/uup-converter-wimlib.7z "https://git.uupdump.net/uup-dump/misc/raw/branch/master/uup-converter-wimlib.7z"
    7z x -opackages/uup-converter-wimlib packages/uup-converter-wimlib.7z
    rm packages/uup-converter-wimlib.7z
fi

echo "$name" > "$verfile"
