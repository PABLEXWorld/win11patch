# Gawk

set -e

if [ ! -f packages/awk.exe ]; then
    wget -O packages/gawk-3.1.6-1-bin.zip "https://sourceforge.net/projects/gnuwin32/files/gawk/3.1.6-1/gawk-3.1.6-1-bin.zip/download"
    if [ -f packages/awk.exe ]; then rm packages/awk.exe; fi
    unzip packages/gawk-3.1.6-1-bin.zip bin/awk.exe
    mv bin/awk.exe packages
    rmdir bin
    rm packages/gawk-3.1.6-1-bin.zip
fi
