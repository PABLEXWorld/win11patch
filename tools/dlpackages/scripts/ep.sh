# ExplorerPatcher

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" ep

source "$(dirname "${BASH_SOURCE[0]}")/utils/get-github.sh" valinet/ExplorerPatcher 0 "$version"

echo "$newver" > "$verfile"

if [ -f packages/ep_setup.exe ]; then
    if [ -d packages/ep_setup ]; then rm -r packages/ep_setup; fi
    mkdir packages/ep_setup

    pushd packages/ep_setup
        wine ../ep_setup.exe /extract
    popd

    rm packages/ep_setup.exe
fi
