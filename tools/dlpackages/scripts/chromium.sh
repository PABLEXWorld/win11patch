# Ungoogled Chromium

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" chromium
version="Chromium v$version - ungoogled"
source "$(dirname "${BASH_SOURCE[0]}")/utils/fname.sh" chromium

source "$(dirname "${BASH_SOURCE[0]}")/utils/get-github.sh" ungoogled-software/ungoogled-chromium-windows 0 "$version"
if [ -n "$filename_old" ] && [ ! "$filename" == "$filename_old" ]; then rm "packages/$filename_old"; fi

newver=${newver:-10:12}

echo "$newver" > "$verfile"
echo "$filename" > "$oldfnfile"
