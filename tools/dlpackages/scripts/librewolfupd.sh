# LibreWolf WinUpdater

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" librewolfupd
source "$(dirname "${BASH_SOURCE[0]}")/utils/fname.sh" librewolfupd

source "$(dirname "${BASH_SOURCE[0]}")/utils/get-codeberg.sh" ltguillaume/librewolf-winupdater 1 "$version"
if [ -n "$filename_old" ] && [ ! "$filename" == "$filename_old" ]; then rm -r "packages/${filename_old%.zip}"; fi

echo "$newver" > "$verfile"
echo "$filename" > "$oldfnfile"

filename=$(echo "$filename" | urlencode -d)

if [ -f "packages/$filename" ]; then
    unzip -o "packages/$filename" -d "packages/${filename%.zip}"
    rm "packages/$filename"
fi
