# The Bat To Exe Converter (originally at f2ko.de/en/b2e.php) has been abandoned indefinitely and is no longer easily available.
# Bat To Exe Converter was made by Fatih Kodak (github.com/99fk)
# Official IPFS download info from Web Archive of github.com/99fk/Bat-To-Exe-Converter-Downloader:
# Version: 3.2.0.0
# Size: 7.95 MB
# Last updated: 2019-07-30
# MD5: 046744906a71e727676f8b6ffb21bb22
# IPFS URL: QmPBp7wBSC9GukPUcp7LXFCGXBvc2e45PUfWUbCJzuLG65
# "https://ipfs.io/ipfs/QmPBp7wBSC9GukPUcp7LXFCGXBvc2e45PUfWUbCJzuLG65"

if [ ! -d Bat_To_Exe_Converter ]; then
    wget -O Bat_To_Exe_Converter.zip "https://github.com/tokyoneon/B2E/raw/master/Bat_To_Exe_Converter.zip"
    unzip -o Bat_To_Exe_Converter.zip -d Bat_To_Exe_Converter
    rm Bat_To_Exe_Converter.zip
fi

