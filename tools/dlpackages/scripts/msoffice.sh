# Microsoft Office 365 Apps for Enterprise (Spanish)

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" odt
odtversion=${version%.exe}

C2RReleaseData=$(curl "https://mrodevicemgr.officeapps.live.com/mrodevicemgrsvc/api/v2/C2RReleaseData/\?audienceFFN=492350f6-3a01-4f97-b9c0-c7c6ddf67d60" | grep AvailableBuild)
o16build=${C2RReleaseData:21:-3}

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" msoffice

dlScript="http://officecdn.microsoft.com.edgesuite.net/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/Office/Data/v32.cab
  out=Office/Data/v32.cab

http://officecdn.microsoft.com.edgesuite.net/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/Office/Data/v64.cab
  out=Office/Data/v64.cab

http://officecdn.microsoft.com.edgesuite.net/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/Office/Data/v64_$o16build.cab
  out=Office/Data/v64_$o16build.cab

http://officecdn.microsoft.com.edgesuite.net/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/Office/Data/$o16build/i640.cab
  out=Office/Data/$o16build/i640.cab

http://officecdn.microsoft.com.edgesuite.net/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/Office/Data/$o16build/i640.cab.cat
  out=Office/Data/$o16build/i640.cab.cat

http://officecdn.microsoft.com.edgesuite.net/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/Office/Data/$o16build/i643082.cab
  out=Office/Data/$o16build/i643082.cab

http://officecdn.microsoft.com.edgesuite.net/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/Office/Data/$o16build/s640.cab
  out=Office/Data/$o16build/s640.cab

http://officecdn.microsoft.com.edgesuite.net/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/Office/Data/$o16build/s643082.cab
  out=Office/Data/$o16build/s643082.cab

http://officecdn.microsoft.com.edgesuite.net/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/Office/Data/$o16build/sp643082.cab
  out=Office/Data/$o16build/sp643082.cab

http://officecdn.microsoft.com.edgesuite.net/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/Office/Data/$o16build/stream.x64.es-ES.dat
  out=Office/Data/$o16build/stream.x64.es-ES.dat

http://officecdn.microsoft.com.edgesuite.net/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/Office/Data/$o16build/stream.x64.x-none.dat
  out=Office/Data/$o16build/stream.x64.x-none.dat

http://officecdn.microsoft.com.edgesuite.net/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/Office/Data/$o16build/stream.x64.x-none.dat.cat
  out=Office/Data/$o16build/stream.x64.x-none.dat.cat"

if [ "" == "$version" ] || [ ! "$o16build" == "$version" ]; then
    if [ -d "packages/es-ES_Office_Current_x64_v$version" ]; then rm -r "packages/es-ES_Office_Current_x64_v$version"; fi
    directoryprefix=packages/es-ES_Office_Current_x64_v$o16build

    echo "$dlScript" | aria2c --no-conf --log-level=info -x16 -s16 -j5 -c -R -d"$directoryprefix" --input-file=-
    sed -e "s|@o16build@|$o16build|g" tools/dlpackages/res/configure64.xml > "$directoryprefix/configure64.xml"
    sed -e "s|@o16build@|$o16build|g" tools/dlpackages/res/package.info > "$directoryprefix/package.info"

    cp tools/dlpackages/res/firewall.bat "$directoryprefix"
    cp "packages/$odtversion/setup.exe" "$directoryprefix"
    cp tools/dlpackages/res/start_setup.cmd "$directoryprefix"
fi

echo "$o16build" > "$verfile"
