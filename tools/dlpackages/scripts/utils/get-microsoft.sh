set -e

link=$(curl "https://www.microsoft.com/en-us/download/confirmation.aspx?id=$1" | xmllint --html --xpath "string(//span[@class='file-link-view1']/a/@href)" - 2> /dev/null)
export name
name=$(basename "$link")

if [ "" == "$2" ] || [ ! "$name" == "$2" ]; then
    if [ -f "packages/$name" ]; then rm "packages/$name"; fi
    wget --directory-prefix=packages "$link"
fi

echo "$name"
