set -e

relinfo=$(curl -s "https://api.github.com/repos/$1/releases/latest")
name=$(echo "$relinfo" | jq -r ".tag_name")
file=$(echo "$relinfo" | jq -r ".assets[$2].browser_download_url")
export filename
filename="$(basename "$file")"

if [ "" == "$3" ] || [ ! "$name" == "$3" ]; then
    if [ -f "packages/$file" ]; then rm "packages/$file"; fi
    wget --directory-prefix=packages "$file"
fi

export newver="$name"
