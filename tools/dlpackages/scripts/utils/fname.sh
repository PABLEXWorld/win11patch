export oldfnfile=packages/pkgver/$1_filename.txt
unset filename_old
export filename_old

if [ -f "$oldfnfile" ]; then
    filename_old=$(cat "$oldfnfile")
fi
