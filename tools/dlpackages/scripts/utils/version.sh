export verfile=packages/pkgver/$1_version.txt
unset version
export version

if [ -f "$verfile" ]; then
    version=$(cat "$verfile")
fi
