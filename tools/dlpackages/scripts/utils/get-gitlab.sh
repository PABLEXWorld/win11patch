set -e

relinfo=$(curl -s "https://gitlab.com/api/v4/projects/$1/releases")
relinfo=$(echo "$relinfo" | jq -r "first(.[] | select(.upcoming_release == false))")
name=$(echo "$relinfo" | jq -r ".tag_name")
count=$(echo "$relinfo" | jq -r ".assets.count")

for ((i=count-1;i>=0;i--))
do
    file=$(echo "$relinfo" | jq -r ".assets.links[$i].url")
    export filename
    filename="$(basename "$file")"
    if [[ $filename =~ $2 ]]; then
        break
    fi
done

if [ "" == "$3" ] || [ ! "$name" == "$3" ]; then
    if [ -f "packages/$file" ]; then rm "packages/$file"; fi
    wget --directory-prefix=packages "$file"
fi

export newver="$name"
