# Dism++

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" dismpp
version="Dism++ $version"
source "$(dirname "${BASH_SOURCE[0]}")/utils/fname.sh" dismpp

source "$(dirname "${BASH_SOURCE[0]}")/utils/get-github.sh" Chuyu-Team/Dism-Multi-language 0 "$version"
if [ -n "$filename_old" ] && [ ! "$filename" == "$filename_old" ]; then rm -r "packages/${filename_old%.zip}"; fi
newver=${newver:7}

echo "$newver" > "$verfile"
echo "$filename" > "$oldfnfile"

filename=$(echo "$filename" | urlencode -d)

if [ -f "packages/$filename" ]; then
    unzip -o "packages/$filename" -d "packages/${filename%.zip}"
    rm "packages/$filename"
fi
