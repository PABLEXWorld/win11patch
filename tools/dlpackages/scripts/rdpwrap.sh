# RDP Wrapper

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" rdpwrap
version="v$version"

source "$(dirname "${BASH_SOURCE[0]}")/utils/get-github.sh" sebaxakerhtc/rdpwrap 0 "$version"
newver=${newver:1}

echo "$newver" > "$verfile"

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" rdpwrapini

name=$(curl "https://api.github.com/repos/sebaxakerhtc/rdpwrap.ini/commits" | jq -r ".[0].sha")

if [ "" == "$version" ] || [ ! "$name" == "$version" ]; then
    if [ -f packages/rdpwrap.ini ]; then rm packages/rdpwrap.ini; fi
    wget --directory-prefix=packages "https://github.com/sebaxakerhtc/rdpwrap.ini/raw/master/rdpwrap.ini"
fi

echo "$name" > "$verfile"
