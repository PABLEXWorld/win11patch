# 7-Zip

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" 7z

link=$(curl "https://7-zip.org/")
link="https://7-zip.org/$(echo "$link" | xmllint --html --xpath 'string(//*[contains(text(),"Download 7-Zip")]/following::table//*[contains(text(),"64-bit x64")]/..//*[contains(text(),"Download")]/@href)' -)"
name=$(basename "$link")
name=${name:2:-8}

if [ "" == "$version" ] || [ ! "$name" == "$version" ]; then
    if [ -f "packages/7z$version-x64.exe" ]; then rm "packages/7z$version-x64.exe"; fi
    wget --directory-prefix=packages "$link"
fi

echo "$name" > "$verfile"
