# MSMG Toolkit

set -e

source "$(dirname "${BASH_SOURCE[0]}")/utils/version.sh" msmgtk

file=$(curl "https://msmgtoolkit.in/downloads.html")
name=$(echo "$file" | xmllint --html --xpath 'string(//*[contains(text(),"MSMG Toolkit")]/following::*[contains(text(),"Name: ")])' - 2> /dev/null)
IFS=$'\n' read -ra data <<< "$name"
name=${data:6:-1}


if [ "" == "$version" ] || [ ! "$name" == "$version" ]; then
    link=$(echo "$file" | xmllint --html --xpath 'string(//*[contains(text(),"Name: ")]/following::*[contains(text(),"Download")]/@href)' - 2> /dev/null)
    link=${link:28}

    folderinfo=$(curl -s "https://download.ru/folders/$link.json?page=1&order=created_at&direction=desc")
    link=$(echo "$folderinfo" | jq -r ".contents[] | select(.name == \"$name\").secure_url")

    wget -O "packages/$name" "https://download.ru$link?inline=false"
    if [ -f packages/ToolKitHelper.exe ]; then rm packages/ToolKitHelper.exe; fi
    7z e -opackages "packages/$name" Bin/ToolKitHelper.exe
    rm "packages/$name"
fi

echo "$name" > "$verfile"
