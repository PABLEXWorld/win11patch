#!/bin/bash
set -e

wimlib-imagex extract "out/install.wim" 1 "/Windows/System32/dbghelp.dll" --no-acls --dest-dir="out"
cp packages/pdbdump.exe out

mkdir -p out/symbols
cat tools/dlsym/symfn_explorer.txt | WINEDLLOVERRIDES="dbghelp=n" wine out/pdbdump.exe -csv -r packages/symbols/explorer.pdb/*/explorer.pdb > out/symbols/symbols_explorer.csv
cat tools/dlsym/symfn_startdocked.txt | WINEDLLOVERRIDES="dbghelp=n" wine out/pdbdump.exe -csv -r packages/symbols/StartDocked.pdb/*/StartDocked.pdb > out/symbols/symbols_startdocked.csv
cat tools/dlsym/symfn_startui.txt | WINEDLLOVERRIDES="dbghelp=n" wine out/pdbdump.exe -csv -r packages/symbols/StartUI.pdb/*/StartUI.pdb > out/symbols/symbols_startui.csv
cat tools/dlsym/symfn_twinui.pcshell.txt | WINEDLLOVERRIDES="dbghelp=n" wine out/pdbdump.exe -csv -r packages/symbols/twinui.pcshell.pdb/*/twinui.pcshell.pdb > out/symbols/symbols_twinui.pcshell.csv
rm out/pdbdump.exe
rm out/dbghelp.dll
