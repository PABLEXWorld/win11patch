#!/bin/bash
source tools/uupconv/uupconv_getfirstmeta.sh
set -e
IFS=$'\n'
export WIMLIB_IMAGEX_IGNORE_CASE=1

wimlib-imagex extract "$firstMetadata" 3 "/Windows/System32/msdelta.dll" --no-acls --nullglob --dest-dir="out"
wimlib-imagex extract "$firstMetadata" 3 "/Windows/System32/Cabinet.dll" --no-acls --nullglob --dest-dir="out"
wimlib-imagex extract "$firstMetadata" 3 "/Windows/System32/dpx.dll" --no-acls --nullglob --dest-dir="out"
wimlib-imagex extract "$firstMetadata" 3 "/Windows/System32/expand.exe" --no-acls --nullglob --dest-dir="out"
