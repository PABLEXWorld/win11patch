#!/bin/bash
set -e

export tempDir=`mktemp -d`

wimlib-imagex extract "out/install.wim" 1 @"tools/dlsym/symlist.txt" --dest-dir="$tempDir"
if [ -d "packages/symbols" ]; then rm -r packages/symbols; fi
mkdir packages/symbols
packages/pdblister manifest "$tempDir" packages/pkgver/symbols.txt
packages/pdblister download "SRV*packages/symbols*https://msdl.microsoft.com/download/symbols" packages/pkgver/symbols.txt

rm -rf "$tempDir"
