@echo off
setlocal enabledelayedexpansion
reg load HKLM\DEFAULT "C:\Users\Default\NTUSER.DAT" >nul
for /f "tokens=1 delims=" %%a in ('dir /S /B regmod') do (
    if %%~xa==.reg (
        set "filename=%%~na"
        if not "!filename:~0,1!"=="#" (
           call :parsereg "%%a"
        )
    )
)
reg unload HKLM\DEFAULT >nul
endlocal
goto :eof

:keyoverrides
set "reglinekeycleaned=!reglinekeycleaned:HKEY_LOCAL_MACHINE\DEFAULT=HKLM\DEFAULT!"
set "reglinekeycleaned=!reglinekeycleaned:HKEY_LOCAL_MACHINE\SOFTWARE2=HKLM\SOFTWARE!"
set "reglinekeycleaned=!reglinekeycleaned:HKEY_LOCAL_MACHINE\SYSTEM2=HKLM\SYSTEM!"
set "reglinekeycleaned=!reglinekeycleaned:ControlSet001=CurrentControlSet!"
goto :eof

:keyexists
reg query "%~1" 1>nul 2>nul
if ERRORLEVEL 1 (
    echo ERROR: Clave %~1 no existe
    set err=1
    set errkey=1
)
goto :eof

:valueexists
if "%~2"=="@" (
    reg query "%~1" /ve 1>nul 2>nul
) else (
    reg query "%~1" /v "%~2" 1>nul 2>nul
)
if ERRORLEVEL 1 (
    echo ERROR: Clave %~1\%~2 no existe
    set err=1
    set errvalue=1
)
goto :eof

:keynotexists
reg query "%~1" 1>nul 2>nul
if NOT ERRORLEVEL 1 (
    echo ERROR: Clave %~1 existe
    set err=1
)
goto :eof

:valuenotexists
if "%~2"=="@" (
    reg query "%~1" /ve 1>nul 2>nul
) else (
    reg query "%~1" /v "%~2" 1>nul 2>nul
)
if NOT ERRORLEVEL 1 (
    echo ERROR: Clave %~1\%~2 existe
    set err=1
)
goto :eof

:checkvalue
if "%~2"=="@" (
    set "command=reg query "%~1" /ve"
) else (
    set "command=reg query "%~1" /v "%~2""
)
for /f "skip=2 tokens=1,3*" %%a in ('%command%') do (
    set "pcval=%%~b"
    set "regval=%~3"

    if "!regval:~0,5!"=="dword" (
        set "pcval=!pcval:~2!"
        set "pcval=00000000!pcval!"
        set "pcval=!pcval:~-8!"
        set "regval=!regval:~6!"
    )
    if NOT "!pcval!"=="!regval!" (
        if "!errmatch!"=="0" (
            echo [%~1]
            set errmatch=1
        )
        echo %~2 -^> "!pcval!" :: "!regval!"
        set err=1
    )
)
goto :eof

:parsereg
set err=0
set errkey=0
for /f "tokens=1 eol=; delims=" %%a in ('type %1') do (
    set regline=%%a

    :: If line is key
    if "!regline:~0,1!"=="[" (

        :: If line is delete key
        if "!regline:~1,1!"=="-" (
            set reglinekeycleaned=!regline:~2,-1!
            call :keyoverrides
            call :keynotexists "!reglinekeycleaned!"
            if "!reglinekeycleaned:~0,12!"=="HKLM\DEFAULT" call :keynotexists "!reglinekeycleaned:HKLM\DEFAULT=HKCU!"
        ) else (
            set reglinekeycleaned=!regline:~1,-1!
            call :keyoverrides
            call :keyexists "!reglinekeycleaned!"
            if "!reglinekeycleaned:~0,12!"=="HKLM\DEFAULT" call :keyexists "!reglinekeycleaned:HKLM\DEFAULT=HKCU!"
        )

    ) else if NOT "!regline:~0,1!"=="W" (
        if !errkey!==0 (
            set errmatch=0
            for /f "tokens=1,2 delims==" %%b in ("!regline!") do (
                if "%%c"=="-" (
                    call :valuenotexists "!reglinekeycleaned!" "%%~b"
                    if "!reglinekeycleaned:~0,12!"=="HKLM\DEFAULT" call :valuenotexists "!reglinekeycleaned:HKLM\DEFAULT=HKCU!" "%%~b"
                ) else (
                    set errvalue=0
                    call :valueexists "!reglinekeycleaned!" "%%~b"
                    if "!reglinekeycleaned:~0,12!"=="HKLM\DEFAULT" call :valueexists "!reglinekeycleaned:HKLM\DEFAULT=HKCU!" "%%~b"
                    if !errvalue!==0 (
                        call :checkvalue "!reglinekeycleaned!" "%%~b" "%%~c"
                        set errmatch=0
                        if "!reglinekeycleaned:~0,12!"=="HKLM\DEFAULT" call :checkvalue "!reglinekeycleaned:HKLM\DEFAULT=HKCU!" "%%~b" "%%~c"
                    )
                )
            )
        )
    )
)
if !err!==1 (
    set "dir=%~1"
    set "dir=!dir:%~dp0regmod\=!"
    echo Archivo: !dir!
)
