#!/bin/bash
# Based on Multiplatform UUP Converter v0.7.2
# https://github.com/uup-dump/converter
source tools/uupconv/uupconv_getfirstmeta.sh

if [ -e out/intermediates/ISODIR ]; then
  rm -rf out/intermediates/ISODIR
fi

export WIMLIB_IMAGEX_IGNORE_CASE=1
mkdir -p out/intermediates/ISODIR

echo -e "$infoColor""Creating WinPE ISO structure...""$resetColor"

wimlib-imagex extract "$firstMetadata" 1 @"tools/uupconv/bootfiles.txt" --no-acls --no-attributes --nullglob --dest-dir=out/intermediates/ISODIR 2>/dev/null
errorHandler $? "Failed to create WinPE ISO structure"

echo ""
echo -e "$infoColor""Exporting winre.wim...""$resetColor"

wimlib-imagex export "$firstMetadata" 2 "out/winpe.wim" \
  --compress=maximum --boot

errorHandler $? "Failed to export winre.wim"

echo ""
echo -e "$infoColor""Creating winpe.wim...""$resetColor"

wimlib-imagex info out/winpe.wim 1 "Microsoft Windows PE" "Microsoft Windows PE" \
  --image-property FLAGS=9 >/dev/null

wimlib-imagex extract out/winpe.wim 1 --dest-dir="$tempDir" \
  "/Windows/System32/config/SOFTWARE" --no-acls >/dev/null

errorHandler $? "Failed to extract registry"

echo 'cd Microsoft\Windows NT\CurrentVersion
nv 1 SystemRoot
ed SystemRoot
X:\$Windows.~bt\Windows
cd WinPE
nv 1 InstRoot
ed InstRoot
X:\$Windows.~bt
q
y' | chntpw -e "$tempDir/SOFTWARE" >/dev/null

wimlib-imagex update out/winpe.wim 1 \
  --command "add $tempDir/SOFTWARE /Windows/System32/config/SOFTWARE" >/dev/null

wimlib-imagex extract out/winpe.wim 1 "/Windows/System32/winpe.jpg" \
  --no-acls --dest-dir="out/intermediates/ISODIR/sources" >/dev/null 2>/dev/null

bckimg=background_cli.bmp
if [ -e ./out/intermediates/ISODIR/sources/background_svr.bmp ]; then
  bckimg=background_svr.bmp
elif [ -e ./out/intermediates/ISODIR/sources/background_cli.png ]; then
  bckimg=background_cli.png
elif [ -e ./out/intermediates/ISODIR/sources/background_svr.png ]; then
  bckimg=background_svr.png
elif [ -e ./out/intermediates/ISODIR/sources/winpe.jpg ]; then
  bckimg=winpe.jpg
fi

wimlib-imagex update out/winpe.wim 1 \
  --command "add out/intermediates/ISODIR/sources/$bckimg /Windows/system32/winpe.jpg" >/dev/null

wimlib-imagex update out/winpe.wim 1 \
  --command "add out/intermediates/ISODIR/sources/$bckimg /Windows/system32/winre.jpg" >/dev/null

wimlib-imagex update out/winpe.wim 1 \
  --command "delete /Windows/System32/winpeshl.ini" >/dev/null

rm -rf "out/intermediates/ISODIR/sources"

cleanup
