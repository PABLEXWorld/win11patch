#!/bin/bash
# Based on Multiplatform UUP Converter v0.7.2
# https://github.com/uup-dump/converter
source tools/uupconv/uupconv_head.sh

list=
for i in $editions; do
    list="$list -ie \"$i""_..-.*.esd\""
done

metadataFiles=$(find "$UUPs_main" 2>/dev/null | eval grep $list)
if [ $? != 0 ]; then
  echo -e "$errorColor""No metadata ESDs found.""$resetColor"
  exit 1
fi

list=

firstMetadata=$(head -1 <<< "$metadataFiles")
getLang=`wimlib-imagex info "$firstMetadata" 3`
lang=`grep -i "^Default Language:" <<< "$getLang" | sed "s/.*  //g"`
#lang=$(grep -i "_..-.*.esd" <<< "$metadataFiles" | head -1 | tr '[:upper:]' '[:lower:]' | sed 's/.*_//g;s/.esd//g')
metadataFiles=$(grep -i "$lang" <<< "$metadataFiles" | sort | uniq)
export firstMetadata=$(head -1 <<< "$metadataFiles")
