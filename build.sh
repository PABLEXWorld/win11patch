#!/bin/bash
set -e
source envsetup.sh

./buildpackages.sh
./dlpackages.sh

pushd tools/adsorber
    ./fakeroot_adsorber.sh
popd

tools/uupconv/uupconv_winpe.sh
targets/winpe/winpe_startnet.sh
targets/winpe/mkiso_winpe.sh winpe

./runtarget.sh qemu_tmpfs winpemod
./runtarget.sh qemu uupconv_win
./runtarget.sh qemu_tmpfs winpemod_second

targets/winre/mkiso_winre.sh

tools/dlsym/dlsym.sh
tools/dlsym/parsesym.sh

./runtarget.sh qemu main_$mode
./qemu_install.sh

./runtarget.sh qemu_install_post main_final
