#!/bin/bash
set -e

if [ ! -d packages/pkgver ]; then mkdir packages/pkgver; fi

find tools/dlpackages/scripts/*.sh -type f -not -iname "msoffice.sh" -not -iname "viowin.sh" | parallel -j5 {}

tools/dlpackages/scripts/msoffice.sh
