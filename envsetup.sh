#!/bin/bash
export UUPs_main="packages/22631.3880_amd64_es-es_professional_9e4fba0d/UUPs"
export UUPs_apps="packages/22631.3880_amd64_neutral_app_9e4fba0d/UUPs"
export dartiso="packages/mu_microsoft_desktop_optimization_pack_2015_x86_x64_dvd_5975282.iso"
export dartmsi="DaRT/DaRT 10/Installers/es-es/x64/MSDaRT100.msi"
export dartmsiout="v10"
export mode=nodefender
export WINEDEBUG=-all
mkdir -p out/wine
export WINEPREFIX="$(realpath out/wine)"
wineboot -i
