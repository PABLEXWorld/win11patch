#!/bin/bash
set -e

touch "out/shared/mount"

dismpp_version=$(urlencode -d < "packages/pkgver/dismpp_filename.txt")
if [ -f "out/shared/setup.cmd" ]; then rm "out/shared/setup.cmd"; fi
cp "targets/$1/setup.bat" "out/shared/setup.cmd"
sed -i "s/@DISMPP_VERSION@/${dismpp_version%.zip}/g" "out/shared/setup.cmd"
unix2dos "out/shared/setup.cmd"

if [ ! -f "hd.qcow2" ]; then qemu-img create -f qcow2 "hd.qcow2" 64G; fi
set +e
# shellcheck disable=SC2046
cmd=$(qemu-system-x86_64 \
-d guest_errors \
-machine q35 \
-accel kvm \
-cpu host,hv_crash \
-boot order=d \
-smp $(nproc) \
-m 4G \
-cdrom "out/winpe.iso" \
-drive id=disk,file="hd.qcow2",if=none,format=qcow2 \
-device ahci,id=ahci \
-device ide-hd,drive=disk,bus=ahci.0 \
-nic user,smb=$(realpath "out/shared"),restrict=on \
-vga qxl \
-usb \
-device usb-kbd \
-device usb-tablet \
-no-reboot 2>&1)
# shellcheck disable=SC2181
if [ ! "$?" -eq 0 ]; then
  echo QEMU has errored.
  exit 1
fi
set -e
if echo "$cmd" | grep -q 'Guest crashed'; then
  if [ -f "out/shared/mount" ]; then rm "out/shared/mount"; fi
  echo The virtual machine has BSOD\'d.
  exit 1
fi

error=0
if [ -f "out/shared/mount" ]; then rm "out/shared/mount"; error="1"; fi
if [ ! -f "out/shared/setupdone" ]; then error="1"; fi

if [ "$error" -eq 1 ]; then
  echo An error occured with the virtual machine.
  exit 1
fi

rm "out/shared/setupdone"
